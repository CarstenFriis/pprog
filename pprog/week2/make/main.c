#include <limits.h>
#include <stdio.h>
#include <float.h>

int equal(double a, double b, double tau, double epsilon);
void name_digit(int i);
int main() {
	int i=1;
	while(i+1>i){
		i++;
	}
	printf("loop max  :%i\n", i);
	printf("max int limits:%i\n", INT_MAX);

	int n=1;
	while(n-1<i){
		n--;
	}
	printf("loop min  :%i\n", n);
	printf("min int limits:%i\n",INT_MIN);

	printf("double:\n");
	double d1=1.0; 
	while(1+d1!=1 )
	{
		d1=d1/2;
	}
	printf("while:%g\n", d1);

	double d2=1;
	do {
		d2=d2/2;
	}
	while(1+ d2!=1);
	printf("do while:%g\n",d2);

	double d3 = 1;
	for (d3;1+d3!=1;d3=d3/2);

	printf("for loop:%g\n",d3);

	printf("float:\n");
	float f1=1.0f; 
	while(1+f1!=1 )
	{
		f1=f1/2;
	}
	printf("while:%g\n", f1);

	float f2=1;
	do {
		f2=f2/2;
	}
	while(1+ f2!=1);
	printf("do while:%g\n",f2);

	printf("long double: \n");
	long double z=1.0L; 
	while(1+z!=1 )
	{
		z=z/2;
	}
	printf("while %Lg\n", z);
	
	int max = INT_MAX/2;
	int imax = 1;
	float sum_up_float = 0.0f;
	for(imax;imax<=max;imax++){
		sum_up_float += 1.0f/imax;
	}
	printf("sum_up_float = %f\n", sum_up_float);

	float sum_down_float = 0.0f;
	for(int imin=0;imin<=max-1;imin++){
		sum_down_float += 1.0f/(max-imin);
	}
	printf("sum_down_float = %f\n", sum_down_float);
	int e1;
	e1 = equal(1,2,0,0.0002);
	printf("%i\n",e1 );
	int e2;
	e2 = equal(1,1,0,0.00002);
	printf("%i\n",e2 );

	printf("name_digit\n");

	printf("1:");
	name_digit(1);

	printf("4:");
	name_digit(4);
	printf("5:");
	name_digit(5);

	return 0;
}
