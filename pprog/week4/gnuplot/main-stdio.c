#include <stdlib.h>
#include <stdio.h>
#include <tgmath.h>

int main(int argc, char const *argv[])
{
	//printf("x \t cos(x)\n");
	double x;
	while( scanf("%lg",&x) != EOF ) printf("%lg \t %lg\n",x,cos(x));
	return 0;
}