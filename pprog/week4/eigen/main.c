#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_sf_airy.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>

void matrix_fprintf(FILE *mystream, char *s, const gsl_matrix *A)
{
	int i, j;
	int n, m;

	n = (*A).size1;
	m = (*A).size2;
	fprintf(mystream, "%s\n", s);
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++)
		{
			fprintf(mystream, "%g \t ", gsl_matrix_get(A, i, j));
		}
		fprintf(mystream, "\n");
	}
}

int main(void)
{
	double x, ai, bi;
	int n = 3;
	printf(" x \t Ai \t Bi ");
	for (x = -15; x <= 5; x += 0.05)
	{
		ai = gsl_sf_airy_Ai(x, GSL_PREC_DOUBLE);
		bi = gsl_sf_airy_Bi(x, GSL_PREC_DOUBLE);
		printf(" %g \t %g \t %g \n", x, ai, bi);
	}
	int i;
	FILE *mystream1 = fopen("matrix.txt", "w");
	fprintf(mystream1, "\n part 2. Linear Equations \n");
	gsl_vector *b = gsl_vector_calloc(n);
	gsl_vector_set(b, 0, 6.32);
	gsl_vector_set(b, 1, 5.37);
	gsl_vector_set(b, 2, 2.29);
	fprintf(mystream1, "\nVector:\n");
	gsl_vector_fprintf(mystream1, b, "%g");

	printf("\n");
	gsl_matrix *m = gsl_matrix_calloc(n, n);

	gsl_matrix_set(m, 0, 0, 6.13);
	gsl_matrix_set(m, 1, 0, 8.08);
	gsl_matrix_set(m, 2, 0, -4.36);
	gsl_matrix_set(m, 0, 1, -2.90);
	gsl_matrix_set(m, 1, 1, -6.31);
	gsl_matrix_set(m, 2, 1, 1.00);
	gsl_matrix_set(m, 0, 2, 5.86);
	gsl_matrix_set(m, 1, 2, -3.89);
	gsl_matrix_set(m, 2, 2, 0.19);
	gsl_matrix *A = gsl_matrix_calloc(n, n);

	gsl_matrix_memcpy(A, m);

	matrix_fprintf(mystream1, "Matrix:", m);
	fprintf(mystream1, "\n");
	fprintf(mystream1, "Solution:\n");
	gsl_vector *v = gsl_vector_calloc(n);
	gsl_linalg_HH_solve(m, b, v);
	for (i = 0; i < 3; i++)
	{
		fprintf(mystream1, "x%i = %g\n", i, gsl_vector_get(v, i));
	}

	gsl_vector *y = gsl_vector_calloc(n);
	fprintf(mystream1, "Original Matrix times solution: \n");
	gsl_blas_dgemv(CblasNoTrans, 1, A, v, 0, y);
	/*d for double, ge for generel, mv for matrix-vector product
	the operation is y=aplha*A*b+beta*y, m matrix, b and y vectors.
	The CblasNoTrans means A isnt transponed. We don't want beta*y. The 0 is the value of bet, and 1 is the value of alpha.*/
	gsl_matrix_mul_elements(A,m);
	/*
	fprintf(mystream1, "\nabscsadsa=%i\n", gsl_vector_equal(y, b));
	for (i = 0; i < 3; i++)
	{
		fprintf(mystream1, "b%i = %.25g\n", i, gsl_vector_get(b, i));
	}
	for (i = 0; i < 3; i++)
	{
		fprintf(mystream1, "y%i = %.25g\n", i, gsl_vector_get(y, i));
	}*/
	fclose(mystream1);
	gsl_matrix_free(m);
	gsl_vector_free(v);
	gsl_vector_free(b);
	gsl_vector_free(y);

	FILE *mystream2 = fopen("eig.txt", "w");
	fprintf(mystream2, "\n part 3. Eig of 4th order Hilbert Matrix \n");
	int size = 4;
	gsl_matrix *H = gsl_matrix_calloc(size, size);
	for (double l = 0; l < size; l++)
	{
		for (double k = 0; k < size; k++)
		{
			gsl_matrix_set(H, l, k, 1 / (l + k + 1.0));
		}
	}

	matrix_fprintf(mystream2, "Hilber Matrix: ", H);

	gsl_vector *eval = gsl_vector_alloc(size);
	gsl_matrix *evec = gsl_matrix_calloc(size, size);

	gsl_eigen_symmv_workspace *w = gsl_eigen_symmv_alloc(size);

	gsl_eigen_symmv(H, eval, evec, w);

	gsl_eigen_symmv_sort(eval, evec, GSL_EIGEN_SORT_VAL_ASC);
	fprintf(mystream2, "\nEigenValues:\n");
	fprintf(mystream2, "i  eigval\n");
	for (int k = 0; k < size; k++)
	{
		double calculated = gsl_vector_get(eval, k);
		fprintf(mystream2, "%i   %g\n", k, calculated);
	}

	matrix_fprintf(mystream2, "\nEigenvectors :", evec);

	gsl_vector_free(eval);
	gsl_matrix_free(evec);
	gsl_eigen_symmv_free(w);

	fclose(mystream2);
	gsl_matrix_free(H);
	return 0;
}