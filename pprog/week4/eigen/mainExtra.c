#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_sf_airy.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>

void square_matrix_fprintf(FILE *mystream, char *s, const gsl_matrix *A)
{
	int i, j;
	int n;
	n = (*A).size1;
	fprintf(mystream, "%s\n", s);
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			fprintf(mystream, "%g \t ", gsl_matrix_get(A, i, j));
		}
		fprintf(mystream, "\n");
	}
}
/*void vector_fprintf(FILE *mystream, const gsl_vector *x, int size)
{
	if (z.im >= 0)
	{
		printf("%s %g + %gi \n", s, z.re, z.im);
	}
	else
	{
		printf("%s %g - %gi \n", s, z.re, fabs(z.im));
	}
}*/

int main(void)
{
	double x, ai, bi;
	int n = 3;
	printf(" x \t Ai \t Bi ");
	for (x = -15; x <= 5; x += 0.1)
	{
		ai = gsl_sf_airy_Ai(x, GSL_PREC_DOUBLE);
		bi = gsl_sf_airy_Bi(x, GSL_PREC_DOUBLE);
		printf(" %g \t %g \t %g \n", x, ai, bi);
	}
	int i;
	FILE *mystream1 = fopen("matrix.txt", "w");
	fprintf(mystream1, "\n part 2. Linear Equations \n");
	gsl_vector *b = gsl_vector_calloc(n);
	gsl_vector_set(b, 0, 6.32);
	gsl_vector_set(b, 1, 5.37);
	gsl_vector_set(b, 2, 2.29);
	fprintf(mystream1, "\nVector:\n");
	gsl_vector_fprintf(mystream1, b, "%g");
	//for (int i = 0, i < )
	printf("\n");
	gsl_matrix *m = gsl_matrix_calloc(n, n);

	gsl_matrix_set(m, 0, 0, 6.13);
	gsl_matrix_set(m, 1, 0, 8.08);
	gsl_matrix_set(m, 2, 0, -4.36);
	gsl_matrix_set(m, 0, 1, -2.90);
	gsl_matrix_set(m, 1, 1, -6.31);
	gsl_matrix_set(m, 2, 1, 1.00);
	gsl_matrix_set(m, 0, 2, 5.86);
	gsl_matrix_set(m, 1, 2, -3.89);
	gsl_matrix_set(m, 2, 2, 0.19);

	/*fprintf(mystream1, "\nMatrix:\n");
	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < 3; j++)
		{
			fprintf(mystream1, "%g \t ", gsl_matrix_get(m, i, j));
		}
		fprintf(mystream1, "\n");
	}*/
	square_matrix_fprintf(mystream1, "Matrix:", m);
	fprintf(mystream1, "\n");
	fprintf(mystream1, "Solution:\n");
	gsl_vector *v = gsl_vector_calloc(n);
	gsl_linalg_HH_solve(m, b, v);
	for (i = 0; i < 3; i++)
	{
		fprintf(mystream1, "x%i = %g\n", i, gsl_vector_get(v, i));
	}

	gsl_vector *y = gsl_vector_calloc(n);
	fprintf(mystream1, "Original Matrix times solution: \n");
	gsl_blas_dgemv(CblasTrans, 1, m, v, 0, y); //d for double, ge for generel, mv for matrix-vector product
	/*the operation is y=aplha*m*b+beta*y, m matrix, b and y vectors.
	The CblasNoTrans means M isnt transponed. We don't want beta*y. The 0 is the value of bet, and 1 is the value of alpha.*/
	gsl_vector_fprintf(mystream1, y, "%g");
	fclose(mystream1);

	gsl_matrix_free(m);
	gsl_vector_free(v);
	gsl_vector_free(b);
	gsl_vector_free(y);

	/*printf("part from last year\n");
	int n = 200;
	double s = 1.0/(n+1.0);
	gsl_matrix *H = gsl_matrix_calloc(n, n);
	for (i = 0; i < n - 1; i++)
	{
		gsl_matrix_set(H, i    , i    , -2.0);
		gsl_matrix_set(H, i    , i + 1, 1.0);
		gsl_matrix_set(H, i + 1, i    , 1.0);
	}

	gsl_matrix_scale(H, -1/s/s);
	printf("matrix:\n");
	for (i = 0; i < n - 1; i++)
	{
		for (j = 0; j < n - 1; j++)
		{
			printf("%g \t ", gsl_matrix_get(H, i, j));
		}
		printf("\n");
	}

	gsl_vector* eval = gsl_vector_alloc(n);
	gsl_matrix* evec = gsl_matrix_calloc(n, n);

	gsl_eigen_symmv_workspace *w = gsl_eigen_symmv_alloc(n);

	gsl_eigen_symmv(H, eval, evec, w);

	
	gsl_eigen_symmv_sort(eval, evec, GSL_EIGEN_SORT_VAL_ASC);

	fprintf(stderr, "i   exact   calculated\n");
	for (int k = 0; k < n/3; k++)
	{
		double exact = pi * pi * (k + 1) * (k + 1);
		double calculated = gsl_vector_get(eval, k);
		fprintf(stderr, "%i   %g   %g\n", k, exact, calculated);
	}

	gsl_vector_free(eval);
	gsl_matrix_free(evec);
	gsl_eigen_symmv_free(w);
	gsl_matrix_free(H);*/
	FILE *mystream2 = fopen("eig.txt", "w");
	fprintf(mystream2, "\n part 3. Eig of 4th order Hilbert Matrix \n");
	int size = 4;
	gsl_matrix *H = gsl_matrix_calloc(size, size);
	for (double l = 0; l < size; l++)
	{
		for (double k = 0; k < size; k++)
		{
			gsl_matrix_set(H, l, k, 1 / (l + k + 1.0));
		}
	}

	square_matrix_fprintf(mystream2, "Hilber Matrix: ", H);

	gsl_vector *eval = gsl_vector_alloc(size);
	gsl_matrix *evec = gsl_matrix_calloc(size, size);

	gsl_eigen_symmv_workspace *w = gsl_eigen_symmv_alloc(size);

	gsl_eigen_symmv(H, eval, evec, w);

	gsl_eigen_symmv_sort(eval, evec, GSL_EIGEN_SORT_VAL_ASC);
	fprintf(mystream2, "\nEigenValues:\n");
	fprintf(mystream2, "i  eigval\n");
	for (int k = 0; k < size; k++)
	{
		double calculated = gsl_vector_get(eval, k);
		fprintf(mystream2, "%i   %g\n", k, calculated);
	}

	square_matrix_fprintf(mystream2,"\nEigenvectors :",evec);
	/*
	fprintf(mystream2, "x v0 v1 v2\n");
	fprintf(mystream2, "%g %g %g %g\n", 0.0, 0.0, 0.0, 0.0);
	for (int i = 0; i < size; i++)
	{
		double x = gsl_vector_get(xvec, i);
		double v0 = gsl_matrix_get(evec, i, 0);
		double v1 = gsl_matrix_get(evec, i, 1);
		double v2 = gsl_matrix_get(evec, i, 2);
		fprintf(mystream2, "%g %g %g %g\n", x, v0, v1, v2);
	}
	fprintf(mystream2, "%g %g %g %g\n", 1.0, 0.0, 0.0, 0.0);
	*/
	gsl_vector_free(eval);
	gsl_matrix_free(evec);
	gsl_eigen_symmv_free(w);

	fclose(mystream2);
	gsl_matrix_free(H);
	return 0;
}