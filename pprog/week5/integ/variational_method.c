#include <stdio.h>
#include <tgmath.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_errno.h>

double integrand_Psi(double x, void *params)
{
	double alpha = *(double *)params;
	double integrand = exp(-alpha * x * x);
	return integrand;
}

double integrand_H(double x, void *params)
{
	double alpha = *(double *)params;
	double integrand = (-alpha*alpha*x*x+x*x+alpha)*exp(-alpha*x*x)/2;
	return integrand;
}

double integral(double alpha, double (*integrand)(double x, void *params))
{


	gsl_function F;
	F.function = integrand;
	F.params = (void *)&alpha;
	double acc = 1e-7, eps = 1e-7, limit = 1000,a=0, result, error;

	gsl_integration_workspace *w = gsl_integration_workspace_alloc(limit);
	//int gsl_integration_qagiu(gsl_function * f, double a, double epsabs, double epsrel, size_t limit, gsl_integration_workspace * workspace, double * result, double * abserr)
	int status=gsl_integration_qagiu(&F,a, acc, eps, limit,
						 w, &result, &error);

	gsl_integration_workspace_free(w);
	if(status!=GSL_SUCCESS) return NAN;
	else return 2*result;
}

double expectation_energy(double alpha){
	double energy = integral(alpha,&integrand_H)/integral(alpha,&integrand_Psi);
	return energy;
}
int main(void)
{
	double alpha;
	double a=0.1,b=10, dalpha=0.01;
	for(alpha=a;alpha<b;alpha+=dalpha)
		printf("%g %g\n",alpha,expectation_energy(alpha));
	return 0;
}
