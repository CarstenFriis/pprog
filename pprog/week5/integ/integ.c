#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>

double f(double x, void *params)
{
	double f = log(x) / sqrt(x);
	return f;
}

int main(void)
{
	gsl_integration_workspace *w = gsl_integration_workspace_alloc(1000);

	double expected = -4.0;

	gsl_function F;
	F.function = &f;
	F.params = NULL;
	double a = 0,b=1, acc = 1e-7, eps = 1e-7,limit = 1000, result, error;
	gsl_integration_qags(&F, a, b, acc, eps, limit,
						 w, &result, &error);

	printf("result          = % .18f\n", result);
	printf("exact result    = % .18f\n", expected);
	printf("estimated error = % .18f\n", error);
	printf("actual error    = % .18f\n", result - expected);
	printf("intervals       = %zu\n", w->size);

	gsl_integration_workspace_free(w);

	return 0;
}
