#include <stdio.h>
#include <math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>
#include <getopt.h>

int orbit(double x, const double y[], double f[], void *params)
{
	//(void)(x); /* avoid unused parameter warning */
	double epsilon = *(double *)params;
	f[0] = y[1];
	f[1] = 1 - y[0] + epsilon * y[0] * y[0];
	return GSL_SUCCESS;
}

int main(int argc, char *argv[])
{
	double epsilon = 0, uprime = 0;
	while(1){
	int opt = getopt(argc, argv, "e:p:");
	if(opt == -1) break;
		switch (opt)
		{
		case 'e':
			epsilon = atof(optarg);
			break;
		case 'p':
			uprime = atof(optarg);
			break;
		default: /* ? */ //
			fprintf(stderr, "Usage: %s [-e epsilon] [-p uprime]\n", argv[0]);
			exit(EXIT_FAILURE);
		}
	}

	gsl_odeiv2_system orbit_sys;
	orbit_sys.function = orbit;
	orbit_sys.jacobian = NULL;
	orbit_sys.dimension = 2;
	orbit_sys.params = (void *)&epsilon;

	double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
	double phi_max = 20.5 * M_PI, delta_phi = 0.15;
	gsl_odeiv2_driver *driver =
		gsl_odeiv2_driver_alloc_y_new(&orbit_sys, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);

	double phi_i = 0, y[2] = {1, uprime};
	for (double phi = 0; phi < phi_max; phi += delta_phi)
	{
		int status = gsl_odeiv2_driver_apply(driver, &phi_i, phi, y);
		printf("%g %g\n", phi, y[0]);
		if (status != GSL_SUCCESS)
			fprintf(stderr, "fun: status=%i", status);
	}

	gsl_odeiv2_driver_free(driver);
	return 0;
}