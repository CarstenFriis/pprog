#include <stdio.h>
#include<getopt.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>
#include <tgmath.h>

int schr_equation(double r, const double y[], double f[],void *params)
{
	//(void)(x); /* avoid unused parameter warning */
	//double mu = *(double *)params;/*
	double m=940;
	double hb=197.3; //???
	double k = *(double *)params;
	f[0] = y[1];
	f[1] = y[0]*((2*m/(hb*hb))*50*exp(-1*pow(r/1.7,2))-k*k);
	return GSL_SUCCESS;
}

int main(int argc, char* argv[])
{

	double k = 20;
	gsl_odeiv2_system schr_sys;
	schr_sys.function = schr_equation;
	schr_sys.jacobian = NULL;
	schr_sys.dimension = 2;
	schr_sys.params = NULL;
	schr_sys.params = (void *)&k;

	double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
	double r_max = 10, delta_r = 0.05;
	gsl_odeiv2_driver *driver =
		gsl_odeiv2_driver_alloc_y_new
			(&schr_sys, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);

			
	double ri = 0, y[2] = { 0.0, 1 };
	for (double r = 0; r < r_max; r += delta_r) {
		int status = gsl_odeiv2_driver_apply (driver, &ri, r, y);
		printf ("%g %g\n", r, y[0]);
		if (status != GSL_SUCCESS) fprintf (stderr, "fun: status=%i", status);
		}

	gsl_odeiv2_driver_free (driver);
	return 0;
}