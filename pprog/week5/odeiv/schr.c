#include <stdio.h>
#include <getopt.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_complex_math.h>
#include <math.h>

typedef struct
{
	double m;
	double E;
} schr_param;

double Vr(double r)
{
	double Vr = -50 * exp(-pow(r / 1.7, 2));
	return Vr;
}

int schr_equation(double r, const double y[], double f[], void *params)
{
	//(void)(x); /* avoid unused parameter warning */
	schr_param par = *(schr_param *)params;

	double m = par.m;
	double hb = 197.3; 
	double E = par.E;
	f[0] = y[1];
	f[1] = 2 * m / pow(hb, 2) * Vr(r) * y[0] - E * 2 * m / pow(hb, 2) * y[0];
	return GSL_SUCCESS;
}

int main(int argc, char *argv[])
{
	double k, phase;
	double m = 940;
	double E;
	//double hb = 197.3; 

	printf("E (MeV)  Phase Shift\n");
	for (E = 0; E < 20.0; E += 0.01)
	{
		//for(k = 1; k*k*hb*hb/(2*m)<101; k++){
		//E = k*k*hb*hb/(2*m);
		schr_param params = {m, E};
		gsl_odeiv2_system schr_sys;
		schr_sys.function = schr_equation;
		schr_sys.jacobian = NULL;
		schr_sys.dimension = 2;
		schr_sys.params = (void *)&params;

		double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
		double r_max = 15, delta_r = 0.01;
		gsl_odeiv2_driver *driver =
			gsl_odeiv2_driver_alloc_y_new(&schr_sys, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);

		double ri = 0, y[2] = {0.0, 1};
		for (double r = 0; r < r_max; r += delta_r)
		{
			int status = gsl_odeiv2_driver_apply(driver, &ri, r, y);
			//printf("%g %g\n", r, y[0]);
			if (status != GSL_SUCCESS)
				fprintf(stderr, "fun: status=%i", status);
		}

		k = sqrt(2 * m * E) / 197.3;
		phase = k * r_max - atan(k * y[0] / y[1]);
		printf("%g \t %g\n", E, phase);
		gsl_odeiv2_driver_free(driver);
	}

	return 0;
}