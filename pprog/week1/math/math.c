#include <stdio.h>
#include <tgmath.h>
#include<complex.h>

int main(){

	double gam;
	gam = tgamma(5);
	printf("Gamma Function of 5 = %g\n",gam);
	double bes;
	bes = j1(0.5);
	printf("Bessel Function of first kind of 0.5 = %g\n",bes );

	complex sqrt_result = csqrt(-2);
	printf("Square root of minus 2       = %g + %gi\n",creal(sqrt_result),cimag(sqrt_result));
	printf("Exponential function of i    = %g + %gi\n",creal(exp(1*I)),cimag(exp(1*I)));
	printf("Exponential function of i*pi = %g + %gi\n",creal(cexp(M_PI*1*I)),cimag(cexp(M_PI*1*I)));
	printf("i to the power of e          = %g + %gi\n",creal(cpow(I,M_E)),cimag(cpow(I,M_E)));
	

	printf("Number of digits\n");
	int i = 0.111111111111111111111111111111111111111111111111111111111111111111;
	printf("integer:     %.25i\n",i );
	float f = 0.111111111111111111111111111111111111111111111111111111111111111111f;
	printf("float:       %.25f\n",f);
	double d = 0.111111111111111111111111111111111111111111111111111111111111111111;
	printf("double:      %.25g\n",d );
	long double ld = 0.111111111111111111111111111111111111111111111111111111111111111111L; 
	printf("Long double: %.25Lg\n",ld);

	return 0;
}