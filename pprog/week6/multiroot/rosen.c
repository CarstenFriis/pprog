#include <gsl/gsl_multiroots.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_errno.h>
#include <stdio.h>
int rosenbrock_equation(const gsl_vector *coord, void *params, gsl_vector *f)
{
	double x = gsl_vector_get(coord, 0);
	double y = gsl_vector_get(coord, 1);
	double dfdx = 2*x-2+400*x*x*x-400*y*x; //2*(x-1+200*(x*x*x-y*x));
	double dfdy = 200*y-200*x*x ;//2*(100*y-x*x);
	gsl_vector_set(f, 0, dfdx);
	gsl_vector_set(f, 1, dfdy);
	return GSL_SUCCESS;
}

int main(void)
{
	int dim = 2;
	const gsl_multiroot_fsolver_type *T = gsl_multiroot_fsolver_hybrids;
	gsl_multiroot_fsolver *S = gsl_multiroot_fsolver_alloc(T, dim);

	gsl_multiroot_function F;
	F.f = rosenbrock_equation;
	F.n = 2;
	F.params = NULL;

	gsl_vector *start = gsl_vector_alloc(dim);
	gsl_vector_set(start, 0, 2.0);
	gsl_vector_set(start, 1, -2.0);
	gsl_multiroot_fsolver_set(S, &F, start);
	int flag,iter = 0;
	double acc = 1e-5;
	fprintf(stderr,"iterations  x y dfdx dfdy:\n");
	do
	{
		iter++;
		gsl_multiroot_fsolver_iterate(S);
		flag = gsl_multiroot_test_residual((*S).f, acc);
		double x = gsl_vector_get((*S).x, 0);
		double y = gsl_vector_get((*S).x, 1);
		double fx = gsl_vector_get((*S).f, 0);
		double fy = gsl_vector_get((*S).f, 1);
		fprintf(stderr,"%.5i %.5g %.5g %.5g %.5g\n",iter,x,y,fx,fy);
	} while (flag == GSL_CONTINUE && iter < 1000);

	fprintf(stderr,"\n\n %g  %g\n\n",gsl_vector_get(start,0),gsl_vector_get(start,1));

	fprintf(stderr,"\n\n status = %s\n",gsl_strerror(flag));
	printf("status = %s\n",gsl_strerror(flag));
	
	printf("number of iterations: %i\n",iter);

	double result_x = gsl_vector_get((*S).x, 0);
	double result_y = gsl_vector_get((*S).x, 1);

	printf("root: (%g,%g) \n",result_x,result_y);

	gsl_vector_free(start);
	gsl_multiroot_fsolver_free(S);
	return 0;
}
