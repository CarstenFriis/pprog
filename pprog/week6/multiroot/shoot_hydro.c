#include <stdio.h>
#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>
#include <assert.h>

int S_wave_schr_eq(double r, const double f[], double dfdr[], void *params)
{
	double epsilon = *(double *)params;
	dfdr[0] = f[1];
	dfdr[1] = -2 * f[0] * (epsilon + 1 / r);
	return GSL_SUCCESS;
}

double F_epsilon(double epsilon, double r_max)
{
	assert(r_max >=0);
	const double r_min = 1e-3;
	if (r_max < r_min){
		return r_min-r_min*r_min;
	}
	int dim = 2;
	gsl_odeiv2_system schr_sys;
	schr_sys.function = S_wave_schr_eq;
	schr_sys.jacobian = NULL;
	schr_sys.dimension = dim;
	schr_sys.params = (void *)&epsilon;

	double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;

	gsl_odeiv2_driver *driver =
		gsl_odeiv2_driver_alloc_y_new(&schr_sys, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);

	double r_i = r_min, y[2] = {r_i - r_i * r_i, 1 - 2 * r_i};
	int status = gsl_odeiv2_driver_apply(driver, &r_i, r_max, y);
	if (status != GSL_SUCCESS)
		fprintf(stderr, "fun: status=%i", status);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

int Master_equation(const gsl_vector *eps, void *params, gsl_vector *f)
{
	double r_max = *(double *)params;
	double epsilon = gsl_vector_get(eps, 0);
	double val = F_epsilon(epsilon, r_max);
	gsl_vector_set(f, 0, val);
	return GSL_SUCCESS;
}
int Master_equation_better_bound(const gsl_vector *eps, void *params, gsl_vector *f)
{
	double r_max = *(double *)params;
	double epsilon = gsl_vector_get(eps, 0);
	double r1=r_max,r2=r_max+0.1;
	double f1 = F_epsilon(epsilon, r1);
	double f2 = F_epsilon(epsilon, r2);
	double kappa=sqrt(-2*epsilon);
	double mismatch=f1*r2*exp(-kappa*r2)-f2*r1*exp(-kappa*r1);
	gsl_vector_set(f, 0, mismatch);
	return GSL_SUCCESS;
}
int main(int argc, char* argv[])
{

	double r_max = 10;
	if( argc > 1){
		r_max = atof(argv[1]);
	}
	int dim = 1;
	const gsl_multiroot_fsolver_type *T = gsl_multiroot_fsolver_hybrids;
	gsl_multiroot_fsolver *solver = gsl_multiroot_fsolver_alloc(T, dim);

	gsl_multiroot_function F;
	F.f = Master_equation;
	F.n = dim;
	F.params = (void *)&r_max;
	gsl_vector *start = gsl_vector_alloc(dim);
	double initial_guess = -1;
	gsl_vector_set(start, 0, initial_guess);

	gsl_multiroot_fsolver_set(solver, &F, start);
	int status, iter = 0;
	fprintf(stderr, "r_max = %g, initial_guess = %g:\n",r_max,initial_guess);
	do
	{
		iter++;
		status = gsl_multiroot_fsolver_iterate(solver);
		if (status)
			break;

		status = gsl_multiroot_test_residual((*solver).f, 1e-12);
		if (status == GSL_SUCCESS)
			fprintf(stderr, "converged:\n");

		fprintf(stderr, "iter= %3i ", iter);
		fprintf(stderr, "e= %10g ", gsl_vector_get(solver->x, 0));
		fprintf(stderr, "f(rmax)= %10g ", gsl_vector_get(solver->f, 0));
		fprintf(stderr, "\n");
	} while (status == GSL_CONTINUE && iter < 100);
	double result_eps = gsl_vector_get((*solver).x, 0);
	fprintf(stderr,"status = %s\n",gsl_strerror(status));
	fprintf(stderr, "\n\n");

	printf("r_max epsilon iterations\n");
	printf("%g   %8g %8i\n", r_max, result_eps, iter);

	printf("\n\n");
	printf("   r,   Fe(e,r),       exact\n");
	for (double r = 0; r<=r_max; r+=r_max / 64){
		printf("%3g %12g %12g\n",r, F_epsilon(result_eps,r),r*exp(-r));
	}
	printf("\n\n");
	gsl_vector_free(start);
	gsl_multiroot_fsolver_free(solver);

	// Do the same again, just use the new function with better boundary

	FILE *datastream = fopen("shoot_better_data.txt", "a");
	FILE *logstream = fopen("shoot_better_log.txt", "a");

		//const gsl_multiroot_fsolver_type *T1 = gsl_multiroot_fsolver_hybrids;
	gsl_multiroot_fsolver *solver1 = gsl_multiroot_fsolver_alloc(T, dim);

	gsl_multiroot_function F1;
	F1.f = Master_equation_better_bound;
	F1.n = dim;
	F1.params = (void *)&r_max;
	gsl_vector *start1 = gsl_vector_alloc(dim);

	double initial_guess1 = -1.0;
	gsl_vector_set(start1, 0, initial_guess1);

	gsl_multiroot_fsolver_set(solver1, &F1, start1);
	iter = 0;
	int acc = 1e-12;
	fprintf(logstream,"r_max = %g, initial guess = %g:\n",r_max,initial_guess1);
	do
	{
		iter++;
		status = gsl_multiroot_fsolver_iterate(solver1);
		if (status)
			break;

		status = gsl_multiroot_test_residual((*solver1).f, acc);
		if (status == GSL_SUCCESS)
			fprintf(logstream,"converged:\n");

		fprintf(logstream, "iter= %3i ", iter);
		fprintf(logstream, "e= %10g ", gsl_vector_get(solver1->x, 0));
		fprintf(logstream, "f(rmax)= %10g ", gsl_vector_get(solver1->f, 0));
		fprintf(logstream, "\n");
	} while (status == GSL_CONTINUE && iter < 100);
	fprintf(logstream,"status = %s\n",gsl_strerror(status));
	fprintf(logstream, "\n\n");
	double result_eps1 = gsl_vector_get((*solver1).x, 0);

	fprintf(datastream,"r_max epsilon iterations\n");
	fprintf(datastream,"%g   %8g %8i\n", r_max, result_eps1, iter);

	fprintf(datastream,"\n\n");
	fprintf(datastream,"   r,   Fe(e,r),       exact\n");
	for (double r = 0; r<=r_max; r+=r_max / 64){
		fprintf(datastream,"%8g %12g %12g\n",r, F_epsilon(result_eps1,r),r*exp(-r));
	}
	fprintf(datastream,"\n\n");
	fclose(datastream);
	fclose(logstream);
	gsl_vector_free(start1);
	gsl_multiroot_fsolver_free(solver1);

	return 0;
}
