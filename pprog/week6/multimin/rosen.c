#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multimin.h>
#include <assert.h>

double
rosenbrock_equation(const gsl_vector *v, void *params)
{
	double x, y;
	x = gsl_vector_get(v, 0);
	y = gsl_vector_get(v, 1);

	return (1 - x) * (1 - x) + 100 * (y - x * x) * (y - x * x);
}
void derivative_rosenbrock_equation(const gsl_vector *coord, void *params, gsl_vector *f)
{
	double x = gsl_vector_get(coord, 0);
	double y = gsl_vector_get(coord, 1);
	double dfdx = 2 * x - 2 + 400 * x * x * x - 400 * y * x; //2*(x-1+200*(x*x*x-y*x));
	double dfdy = 200 * y - 200 * x * x;					 //2*(100*y-x*x);
	gsl_vector_set(f, 0, dfdx);
	gsl_vector_set(f, 1, dfdy);
}
void rosenbrock_combined(const gsl_vector *x, void *params,
						 double *f, gsl_vector *df)
{
	*f = rosenbrock_equation(x, params);
	derivative_rosenbrock_equation(x, params, df);
}

int main(int arc, char *argv[])
{
	int iter = 0;
	int status;

	const gsl_multimin_fdfminimizer_type *T;
	gsl_multimin_fdfminimizer *s;
	gsl_multimin_function_fdf rosen_func;

	rosen_func.n = 2;
	rosen_func.f = &rosenbrock_equation;
	rosen_func.df = &derivative_rosenbrock_equation;
	rosen_func.fdf = &rosenbrock_combined;
	rosen_func.params = NULL;
	gsl_vector *start = gsl_vector_alloc(2);
	gsl_vector_set(start, 0, -1);
	gsl_vector_set(start, 1, -2);

	T = gsl_multimin_fdfminimizer_conjugate_fr;
	s = gsl_multimin_fdfminimizer_alloc(T, 2);
	double step_size = 0.01;
	double tolerance = 1e-10;
	gsl_multimin_fdfminimizer_set(s, &rosen_func, start, step_size, tolerance);

	do
	{
		iter++;
		status = gsl_multimin_fdfminimizer_iterate(s);

		if (status)
			break;

		status = gsl_multimin_test_gradient((*s).gradient, 1e-3);

		if (status == GSL_SUCCESS)
			printf("Minimum found at:\n");

		double x = gsl_vector_get((*s).x, 0);
		double y = gsl_vector_get((*s).x, 1);
		printf("%5d %.5f %.5f %10.5f\n", iter,
			   x,
			   y,
			   (*s).f);

	} while (status == GSL_CONTINUE && iter < 1000);
	fprintf(stderr,"status = %s\n",gsl_strerror(status));
	gsl_multimin_fdfminimizer_free(s);
	gsl_vector_free(start);

	return 0;
}