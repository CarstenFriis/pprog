#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multimin.h>
#include <assert.h>
#include <math.h>

typedef struct exp_data
{
    int size;
    double *time, *value, *error;
} exp_data;
double fun(double t, double T, double A, double B)
{
    return A * exp(-t / T) + B;
}
double function_to_minimize(const gsl_vector *x, void *params)
{
    double T = gsl_vector_get(x, 0);
    double A = gsl_vector_get(x, 1);
    double B = gsl_vector_get(x, 2);
    exp_data *data = (exp_data *)params;
    int n = (*data).size;
    double *t = (*data).time;
    double *y = (*data).value;
    double *e = (*data).error;
    double sum = 0;
    for (int i = 0; i < n; i++)
        sum += pow((fun(t[i], T, A, B) - y[i]), 2) / e[i] / e[i];
    return sum;
}

int main()
{
    double t[] = {0.47, 1.41, 2.36, 3.30, 4.24, 5.18, 6.13, 7.07, 8.01, 8.95};
    double y[] = {5.49, 4.08, 3.54, 2.61, 2.09, 1.91, 1.55, 1.47, 1.45, 1.25};
    double e[] = {0.26, 0.12, 0.27, 0.10, 0.15, 0.11, 0.13, 0.07, 0.15, 0.09};
    int n = sizeof(t) / sizeof(t[0]);

    exp_data data;
    data.size = n;
    data.time = t;
    data.value = y;
    data.error = e;

    const int dim = 3;
    gsl_multimin_fminimizer *M = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2, dim);

    gsl_multimin_function F;
    F.f = function_to_minimize;
    F.n = dim;
    F.params = (void *)&data;

    gsl_vector *start = gsl_vector_alloc(dim);
    gsl_vector_set(start, 0, 2);
    gsl_vector_set(start, 1, 4);
    gsl_vector_set(start, 2, 1);

    gsl_vector *step = gsl_vector_alloc(dim);
    gsl_vector_set(step, 0, 0.1);
    gsl_vector_set(step, 1, 0.1);
    gsl_vector_set(step, 2, 0.1);

    gsl_multimin_fminimizer_set(M, &F, start, step);

    do
    {
        gsl_multimin_fminimizer_iterate(M);
        double T = gsl_vector_get((*M).x, 0);
        double A = gsl_vector_get((*M).x, 1);
        double B = gsl_vector_get((*M).x, 2);
        fprintf(stderr, "A=%8.4g E=%9.5g G=%8.3g fval=%9.4g\n", T, A, B, (*M).fval);
        int status = gsl_multimin_test_size(M->size, 1e-4);
        if (status == GSL_SUCCESS)
            break;
    } while (1);

    double A = gsl_vector_get(M->x, 0);
    double E = gsl_vector_get(M->x, 1);
    double G = gsl_vector_get(M->x, 2);

    for (double x = t[0]; x < t[n - 1]; x += 0.02)
        printf("%g %g\n", x, fun(x, A, E, G));

    fprintf(stderr, "A=%g E=%g G=%g\n", A, E, G);

    return 0;
}