#include <stdio.h>
#include <math.h>
#include <getopt.h>
#include <stdlib.h>
double cuberoot(double);
int main(int argc, char *argv[])
{
	
	double xmin = 1, xmax = 10 ,dx = 0.1;
	while (1)
	{
		struct option long_options[] =
			{
				{"xmin", required_argument, NULL, 'a'},
				{"xmax", required_argument, NULL, 'b'},
				{"step", required_argument, NULL, 'd'},
				{0, 0, 0, 0}};
		int opt = getopt_long(argc, argv, "a:b:d", long_options, NULL);
		if (opt == -1)
			break;
		switch (opt)
		{
		case 'a':
			xmin = atof(optarg);
			break;
		case 'b':
			xmax = atof(optarg);
			break;
		case 'd':
			dx = atof(optarg);
			break;
		default:
			fprintf(stderr, "Usage: %s --xmin a --xmax b  --step d\n", argv[0]);
			exit(EXIT_FAILURE);
		}
	}
	for (double x = xmin; x < xmax; x += dx)
		printf("%g %g\n", x, cuberoot(x));

	printf("\n\n");

	for (double x = xmin; x < xmax; x += dx)
		printf("%g %g\n", x, cbrt(x));
	return 0;
}
