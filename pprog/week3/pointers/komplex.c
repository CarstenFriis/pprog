#include <stdio.h>
#include "komplex.h"
#include <tgmath.h>

int equal(double a, double b, double tau, double epsilon);

void komplex_print (char* s, komplex z) {
	if(z.im >= 0) {
	printf("%s %g + %gi \n",s,z.re,z.im); }
	else {
	printf("%s %g - %gi \n",s,z.re,fabs(z.im));
	}
}

void komplex_set (komplex* z, double x, double y){
	(*z).re = x;
	(*z).im = y;
}

komplex komplex_new (double x, double y){
	komplex z = {x,y};
	return z;
}

komplex komplex_add (komplex a,komplex b){
	komplex z;
	z.re = a.re + b.re;
	z.im = a.im + b.im;
	return z;
}

komplex komplex_sub (komplex a,komplex b){
	komplex z;
	z.re = a.re - b.re;
	z.im = a.im - b.im;
	return z;
}

int komplex_equal (komplex a, komplex b,double tau, double epsilon){
	if(equal(a.re,b.re,tau,epsilon) && equal(a.im,b.im,tau,epsilon)){
		return 1;
	}
	else return 0;
}

komplex komplex_mul (komplex a, komplex b) {
	komplex z;
	z.re = (a.re*b.re)-(a.im*b.im);
	z.im = a.re*b.im + a.im*b.re;
	return z;
}

komplex komplex_div (komplex a, komplex b){
	komplex z;
	z.re = (a.re*b.re + a.im*b.im)/(b.re*b.re + b.im*b.im);
	z.im = (b.re*a.im - a.re*b.im)/(b.re*b.re  + b.im*b.im);
	return z;
}

komplex komplex_conjugate (komplex z) {
	komplex x;
	x.re = z.re;
	x.im = -z.im;
	return x;
}

double komplex_abs (komplex z) {
	double x;
	x = sqrt(z.re*z.re+z.im*z.im);
	return x;
}

komplex komplex_exp(komplex z) {
	komplex x;
	x.re = exp(z.re)*(cos(z.im));
	x.im = exp(z.re)*(sin(z.im));
	return x;
}

komplex komplex_sin (komplex z){
	komplex x;
	x.re = sin(z.re)*cosh(z.im);
	x.im = cos(z.re)*sinh(z.im);
	return x;
}

komplex komplex_cos (komplex z){
	komplex x;
	x.re = cos(z.re)*cosh(z.im);
	x.im = -sin(z.re)*sinh(z.im);
	return x;
}


komplex komplex_sqrt (komplex z){
	komplex x;
	x.re = sqrt((z.re + komplex_abs(z))/2);
	x.im = copysign(1,z.im)*sqrt((-z.re + komplex_abs(z))/2);
	//https://en.wikipedia.org/wiki/Complex_number#Square_root
	return x;
}