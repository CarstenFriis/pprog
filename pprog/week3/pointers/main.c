#include <stdio.h>
#include "komplex.h"
#include <tgmath.h>
#include "equal.c"
#define TINY 1e-6

int main(void)
{
	printf("\n");printf("testing komplex_new and komplex_print:\n");
	komplex z=komplex_new(1,0);
	komplex_print("z = ",z);
	double x=5,y=17;
	printf("\n");printf("testing komplex_set:\n");
	printf("x=%g, y=%g\n", x,y);
	komplex_set(&z,x,y);
	printf("setting real(z) = x, imag(z) = y\n");
	komplex_print("z = ",z);

	printf("\n");printf("testing komplex_add:\n");
	komplex w = komplex_new(2,2);
	komplex_print("new komplex number w =",w);
	komplex q = komplex_add(z,w);
	komplex_print("q=w+z is =",q); //q=7+19i
	komplex ADD = {5+2,17+2};
	komplex_print("q=w+z should be =",ADD);

	printf("\n");printf("testing komplex_sub:\n");
	komplex e = komplex_new(2,9);
	komplex_print("new komplex number e =",e);
	komplex r = komplex_sub(q,e); 
	komplex_print("r=q-e is =",r); // r = 5+10i
	komplex SUB = {7-2,17-9};
	komplex_print("q-e should be =",SUB);


	printf("q == z :");
	if ( komplex_equal(q,z,TINY,TINY)) {
		printf("true\n");
	} 
	else {
		printf("false\n");
	}
	komplex d = komplex_new(2,9);
	komplex_print("new komplex number d =",d);

	printf("d == e :");
	if ( komplex_equal(d,e,TINY,TINY)) {
		printf("true\n");
	} 
	else {
		printf("false\n");
	}

	printf("\n");printf("testing komplex_mul:\n");
	komplex j = komplex_mul(w,r);
	komplex_print("j=w*r is =",j);
	komplex MUL = {-10,30};
	komplex_print("w*r should be =",MUL);

	printf("\n");printf("testing komplex_dib:\n");
	komplex g = komplex_div(r,w);
	komplex_print("g=r/w is =",g);
	komplex DIV = {3.75,1.25};
	komplex_print("r/w should be =",DIV);

	printf("\n");printf("testing komplex_conjugate:\n");
	komplex t = komplex_conjugate(w);
	komplex_print("t=conj(w) is =",t);
	komplex CONJ = {2,-2};
	komplex_print("conj(w) should be =",CONJ);

	printf("\n");printf("testing komplex_abs:\n");
	double v = komplex_abs(d);
	printf("v=abs(d) is =%g\n",v);
	double ABS = sqrt(85);
	printf("abs(d) should be=%g\n",ABS);

	printf("\n");printf("testing komplex_exp:\n");
	komplex f = komplex_exp(w);
	komplex_print("f=exp(w) is =",f);
	komplex EXP = {exp(2)*cos(2),exp(2)*sin(2)};
	komplex_print("exp(w) should be =",EXP);


	//sin(a+bi)=sin(a)*cosh(b)+i*cos(a)*sinh(b)
	printf("\n");printf("testing komplex_sin:\n");
	komplex i = komplex_sin(w);
	komplex_print("i=sin(w) is =",i);
	komplex SIN = {sin(2)*cosh(2),cos(2)*sinh(2)};
	komplex_print("SIN(w) should be =",SIN);

	//cos(a+bi)=cos(a)*cosh(b)−i*sin(a)*sinh(b)
	printf("\n");printf("testing komplex_cos:\n");
	komplex u = komplex_cos(w);
	komplex_print("u=cos(w) is =",u);
	komplex COS = {cos(2)*cosh(2),-sin(2)*sinh(2)};
	komplex_print("cos(w) should be =",COS);

	// real(sqrt(a+b*i)) = sqrt((a+sqrt(a^2+b^2))/2))
	// imag(sqrt(a+b*i)) = sgn(b)*sqrt((-a+sqrt(a^2+b^2))/2))
	//https://en.wikipedia.org/wiki/Complex_number#Square_root
	printf("\n");printf("testing komplex_sqrt:\n");
	komplex k = komplex_sqrt(w);
	komplex_print("k=sqrt(w) is =",k);
	komplex SQRT = {sqrt((2+sqrt(2*2+2*2))/2),sqrt((-2+sqrt(2*2+2*2))/2)};
	komplex_print("sqrt(w) should be =",SQRT);



	return 0;
}