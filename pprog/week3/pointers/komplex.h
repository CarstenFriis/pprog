#ifndef HAVE_KOMPLEX_H /* this is necessary for multiple includes */

struct komplex {double re; double im;};
typedef struct komplex komplex;

void    komplex_print    (char* s, komplex z);   /* prints string s and then komplex z */
void komplex_set (komplex* z, double x, double y);
komplex komplex_new (double x, double y);
komplex komplex_add (komplex a,komplex b);
komplex komplex_sub (komplex a,komplex b);
int komplex_equal (komplex a, komplex b,double tau, double epsilon);
komplex komplex_mul (komplex a, komplex b);
komplex komplex_div (komplex a, komplex b);
komplex komplex_conjugate (komplex z);
double komplex_abs (komplex z);

komplex komplex_exp (komplex z);
komplex komplex_sin (komplex z);
komplex komplex_cos (komplex z);
komplex komplex_sqrt (komplex z);
#define HAVE_KOMPLEX_H
#endif