#include "nvector.h"
#include <stdio.h>
#include <stdlib.h>
#define TINY 1e-6
#define RND (double)rand()/RAND_MAX

int main(int argc, char const *argv[])
{
	int n = 5;
	nvector *v = nvector_alloc(n);
	printf("\nmain: testing nvector_alloc ...\n");
	if (v == NULL) printf("test failed\n");
	else printf("test passed\n");

	nvector_set(v,3,5.0);

	nvector *u = nvector_alloc(n);
	for(int i=0; i<n;i++){
		(*u).data[i] = 3*i;
		(*v).data[i] = i;
	}

	
	double dot = nvector_dot_product(u,v);
	printf("ads %g\n",nvector_get(v,3));
	printf("ads %g\n",dot);

	nvector_print("hej", v);
	nvector_set_zero(v);
	nvector_print("Testing nvector_set_zero:", v);
	for(int i=0; i<n;i++){
		(*v).data[i] = i;
	}
	printf("Testing nvector_equal\n");
	nvector_print("1. vector: u=", u);
	nvector_print("2. vector: v=", v);
	printf("u == v :");
	if ( nvector_equal(u,v,TINY,TINY)) {
		printf("true\n");
	} 
	else {
		printf("false\n");
	}

	
	nvector_add(v,u);
	nvector_print("Testing vector addition,v=v+u= ",v);

	nvector_sub(v,u);
	nvector_print("Testing vector subtraction,v=v-u= ",v);

	nvector_scale(v,3.0);
	nvector_print("Testing vector scale,v=v*3= ",v);















	nvector_free(u);
	nvector_free(v);
	
	return 0;
}