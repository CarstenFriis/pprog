#include<stdio.h>
#include"nvector.h"
#include<stdlib.h>
#include <math.h>
#include <assert.h>
#include "equal.c"


void assert_same_size(nvector* u, nvector* v){
  assert((*u).size == (*v).size);
}
void assert_index_in_range(nvector* v,int i){
  assert( 0 <= i && i < (*v).size );
}
int equal(double a, double b, double tau, double epsilon);

nvector* nvector_alloc(int n){
  nvector* v = malloc(sizeof(nvector));
  (*v).size = n;
  (*v).data = malloc(n*sizeof(double));
  if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n");
  return v;
}

void     nvector_free        (nvector* v){
  free(v->data);
  free(v);
}

void     nvector_set(nvector * v, int i, double value){
  assert_index_in_range(v,i);
	(*v).data[i] = value;
}
double   nvector_get         (nvector* v, int i) {
  assert_index_in_range(v,i);
  return (*v).data[i];
}

double   nvector_dot_product (nvector* u, nvector* v){
  assert_same_size(u,v);
  int n = (*u).size;
  double dot = 0;
  for(int i = 0; i<n; i++){
    dot += (*u).data[i]*(*v).data[i];
  }
  return dot;
}


void nvector_print(char* s, nvector* v){
  printf("%s \n",s);
  for(int i = 0; i<(*v).size;i++) {
    printf("Vector index %i = %g\n",i,(*v).data[i]);
  }
}

void nvector_set_zero (nvector* v){
    for(int i = 0; i<(*v).size;i++) {
    nvector_set(v,i,0);
  }
}
int  nvector_equal    (nvector* a, nvector* b,double tau, double epsilon){
  assert_same_size(a,b);
  for(int i = 0; i<(*a).size;i++) {
    if(!equal((*a).data[i],(*b).data[i],tau,epsilon)){
		return 0;
	}
  return 1;
  }
}


void nvector_add      (nvector* a, nvector* b){  
  assert_same_size(a,b);
  for(int i = 0; i<(*a).size;i++) {
    nvector_set(a,i,nvector_get(a,i) + nvector_get(b,i));
  }
}


void nvector_sub      (nvector* a, nvector* b){
  assert_same_size(a,b);
  for(int i = 0; i<(*a).size;i++) {
    nvector_set(a,i,nvector_get(a,i) - nvector_get(b,i));
  }
}

void nvector_scale    (nvector* a, double x){
  for(int i = 0; i<(*a).size;i++) {
    nvector_set(a,i,nvector_get(a,i)*x);
  }
}