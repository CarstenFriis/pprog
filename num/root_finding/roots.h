#ifndef ROOTS_H  /* for multiple includes */
#include"../linear_eq/linear_eq.h"
void System_of_Equations(gsl_vector* p, gsl_vector* fx);
void Rosenbrock(gsl_vector* p, gsl_vector* fx);
void Himmelblau(gsl_vector* p, gsl_vector* fx);
void newton(void f(gsl_vector* x, gsl_vector* fx),gsl_vector* x, double dx, double epsilon,int* steps, int* func_cost);




#define ROOTS_H
#endif