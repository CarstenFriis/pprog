#include"roots.h"


void newton(void f(gsl_vector* x, gsl_vector* fx),gsl_vector* x, double dx, double epsilon,int* steps, int* func_cost){
	int n=x->size;

	gsl_matrix* Q = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_vector* z  = gsl_vector_alloc(n);
	gsl_vector* fz = gsl_vector_alloc(n);
	gsl_vector* df = gsl_vector_alloc(n);
	gsl_vector* Dx = gsl_vector_alloc(n);

	int function_cost = 0;
	int steps_inner=0;

	do{
		function_cost++;
		f(x,fx); 
		
		for (int i=0;i<n;i++){
			gsl_vector_set(x,i,
				gsl_vector_get(x,i)+dx); 
			
			function_cost++;
			f(x,df); 
			gsl_vector_sub(df,fx); 
				
			for(int j=0;j<n;j++){
				gsl_matrix_set(J,j,i,
					gsl_vector_get(df,j)/dx);
			}
			gsl_vector_set(x,i,
				gsl_vector_get(x,i)-dx);
		}
		qr_gs_decomp(J,R);
		qr_gs_solve(J,R,fx,Dx);
		gsl_vector_scale(Dx,-1);
	
		double lambda=1, lambda_min = 0.02; 
		while(1){
			gsl_vector_memcpy(z,x);
			gsl_vector_add(z,Dx);
			function_cost++;
			f(z,fz);

			if( gsl_blas_dnrm2(fz)<(1-lambda/2)*gsl_blas_dnrm2(fx)) break;
			if(lambda < lambda_min ) break;
			lambda*=0.5;
			gsl_vector_scale(Dx,0.5);
			}
		gsl_vector_memcpy(x,z);
		gsl_vector_memcpy(fx,fz);
		steps++;
		} while(gsl_blas_dnrm2(Dx)>dx || gsl_blas_dnrm2(fx)>epsilon);
		
	gsl_matrix_free(J);	
	gsl_matrix_free(Q);	
	gsl_matrix_free(R);
	gsl_vector_free(fx);
	gsl_vector_free(df);
	gsl_vector_free(fz);
	gsl_vector_free(z);
	gsl_vector_free(Dx);

	*steps=steps_inner;
	*func_cost=function_cost;

}