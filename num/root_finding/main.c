#include"roots.h"


int main(void){
	
	int n = 2;
	double epsilon = 1e-6;
	double dx = 1e-6;
	int steps,func_cost;

	gsl_vector* p = gsl_vector_alloc(n);
	gsl_vector* fx = gsl_vector_alloc(n);
	System_of_Equations(p, fx);
	gsl_vector_set(p,0,3);
	gsl_vector_set(p,0,-7);
	printf("Solving system of equation by newton. start at:\n");
	gsl_vector_fprintf(stdout, p, "%g");

	newton(System_of_Equations,p,dx,epsilon,&steps,&func_cost);
	
	printf("root at:\n");
	gsl_vector_fprintf(stdout, p, "%g");

	System_of_Equations(p, fx);

	printf("function value at root:\n");
	gsl_vector_fprintf(stdout, fx, "%g");

	gsl_vector_set_all(p,0.0);

	printf("Solving Rosenbrock. start at:\n");
	Rosenbrock( p, fx);

	gsl_vector_fprintf(stdout, p, "%g"); 
	newton(Rosenbrock,p,dx,epsilon,&steps,&func_cost);

	printf("root at:\n");
	gsl_vector_fprintf(stdout, p, "%g");

	Rosenbrock( p, fx);
	printf("function value at root:\n");
	gsl_vector_fprintf(stdout, fx, "%g");


	gsl_vector_set_all(p,0.0);




	printf("Solving Himmelblau. start at:\n");
	Himmelblau( p, fx);

	gsl_vector_fprintf(stdout, p, "%g"); 
	newton(Himmelblau,p,dx,epsilon,&steps,&func_cost);

	printf("root at:\n");
	gsl_vector_fprintf(stdout, p, "%g");

	Himmelblau( p, fx);
	printf("function value at root:\n");
	gsl_vector_fprintf(stdout, fx, "%g");



	return 0;
}