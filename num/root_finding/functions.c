#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <math.h>


void System_of_Equations(gsl_vector* p, gsl_vector* fx){


	int A = 10000;
	double x=gsl_vector_get(p,0);
	double y=gsl_vector_get(p,1);
	gsl_vector_set(fx,0, A*x*y-1);					
	gsl_vector_set(fx,1, exp(-x)+exp(-y)-1.-1.0/A);	
	}

void Rosenbrock(gsl_vector* p,gsl_vector* fx){
	
	double x=gsl_vector_get(p,0);
	double y=gsl_vector_get(p,1);
	gsl_vector_set(fx,0, -2*(1-x)+400*(y-x*x)*(-1)*x);	
	gsl_vector_set(fx,1, 200*(y-x*x)); 					
	}

void Himmelblau(gsl_vector* p,gsl_vector* fx){
	double x=gsl_vector_get(p,0);
	double y=gsl_vector_get(p,1);
	gsl_vector_set(fx,0, 4*x*(x*x+y-11)+2*(x+y*y-7)); 
	gsl_vector_set(fx,1, 2*(x*x+y-11)+4*y*(x+y*y-7)); 
	}
