#ifndef MONTE_CARLO_H  /* for multiple includes */
#include"../linear_eq/linear_eq.h"
#include<gsl/gsl_rng.h>
void rnd_vector(gsl_rng* r,gsl_vector*a,gsl_vector*b,gsl_vector* x);
void monte_carlo_plain_integration(double f(gsl_vector* x),gsl_vector* a,gsl_vector* b,int N,double* result, double* error);
double nasty_fun(gsl_vector* x);
double fun_square(gsl_vector* x);
#define MONTE_CARLO_H
#endif