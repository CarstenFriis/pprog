#include"monte_carlo.h"

double nasty_fun(gsl_vector* x){
	return (1/(1-cos(gsl_vector_get(x,0))*cos(gsl_vector_get(x,1))*cos(gsl_vector_get(x,2))))/(M_PI*M_PI*M_PI);
}

double fun_square(gsl_vector* x){
	return gsl_vector_get(x,0)*gsl_vector_get(x,0)+gsl_vector_get(x,1)*gsl_vector_get(x,1)+gsl_vector_get(x,2)*gsl_vector_get(x,2);
}
