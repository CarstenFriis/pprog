#include "monte_carlo.h"

void rnd_vector(gsl_rng* r,gsl_vector*a,gsl_vector*b,gsl_vector* x){
	int dim = a->size;
	for(int i=0;i<dim;i++){
		gsl_vector_set(x,i,gsl_vector_get(a,i)+gsl_rng_uniform(r)*(gsl_vector_get(b,i)+gsl_vector_get(a,i)));
	}
}

void monte_carlo_plain_integration(double f(gsl_vector* x),gsl_vector* a,gsl_vector* b,int N, double* result, double* error){

	double vol = 1;
	int dim = a->size;
	for(int i=0;i<dim; i++){
		vol *= (gsl_vector_get(b,i)-gsl_vector_get(a,i));
	}
	const gsl_rng_type *T;
	gsl_rng * r;
	gsl_rng_env_setup();
	T = gsl_rng_default;
	r = gsl_rng_alloc(T);

	double sum=0, sum_squared=0,fx=0;
	gsl_vector* x = gsl_vector_alloc(dim);
	for(int i=0;i<N;i++){
		rnd_vector(r,a,b,x);
		fx=f(x);
		sum+=fx;
		sum_squared += fx*fx;
	}

	double avg = sum/N;
	double variance = sum_squared/N - avg*avg;
	double sigma = sqrt(variance);

	*result = avg*vol;
	*error = sigma*vol/sqrt(N);



	gsl_vector_free(x);
	gsl_rng_free(r);
}