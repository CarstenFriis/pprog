#include"monte_carlo.h"


int main(void){
	int dim = 3;
	gsl_vector* a = gsl_vector_alloc(dim);
	gsl_vector* b = gsl_vector_alloc(dim);
	gsl_vector_set_all(a,0);
	gsl_vector_set_all(b,M_PI);
	double result,error;
	monte_carlo_plain_integration(nasty_fun,a,b,500,&result,&error);

	printf(" Assignment A:");
	printf(" monte carlo plain integral of the given singular integral:");
	printf("result %g\n",result);
	printf("error %g\n",error);
	FILE *mystream = fopen("fit.data", "w");
	gsl_vector_set_all(a,0);
	gsl_vector_set_all(b,M_PI);
	printf("\n\n");
	int m = 1e3;
	for(int n=10; n<m; n++){
		monte_carlo_plain_integration(fun_square,a,b,n,&result,&error);
		fprintf(mystream,"%d \t %g\n",n,error);
	}
	
	printf(" Assignment B:\n");
	printf(" See error.svg for 1/sqrt(N) plot:\n");
	fclose(mystream);
	return 0;


}