#include"multi.h"
#include<time.h>
#include<omp.h>

int main(void){


	double a = 0,b=2000,acc=1e-9,eps=1e-9;
	printf("Adaptive integration by using two threads \n");
	printf("Of function: sqrt(x)*cos(x)*tan(x)*sin(-x) from x=%g to x=%g with acc=%g and eps=%g \n",a,b,acc,eps);
	printf("The result is -29822 \n\n");
	struct timespec start, finish;
	double elapsed;
	clock_gettime(CLOCK_MONOTONIC, &start);
	double sq_int = multi_adapt(my_fun,a,b,acc,eps);
	clock_gettime(CLOCK_MONOTONIC, &finish);
	elapsed = (finish.tv_sec - start.tv_sec);
	elapsed+= (finish.tv_nsec - start.tv_nsec)*1e-9;
	printf("Multiprocessing result:\n");
	printf("%g\n",sq_int);
	printf("time taken: %g\n\n",elapsed);

	struct timespec start2, finish2;
	double elapsed2;
	clock_gettime(CLOCK_MONOTONIC, &start2);
	sq_int = adapt(my_fun,a,b,acc,eps);
	clock_gettime(CLOCK_MONOTONIC, &finish2);
	elapsed2 = (finish2.tv_sec - start2.tv_sec);
	elapsed2+= (finish2.tv_nsec - start2.tv_nsec)*1e-9;
	
	printf("Without Multiprocessing result:\n");
	printf("%g\n",sq_int);
	printf("time taken: %g\n\n",elapsed2);
	return 0;
}