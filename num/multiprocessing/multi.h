#ifndef MULTI_H  /* for multiple includes */
#include"../linear_eq/linear_eq.h"
double adapt_rec(double f(double), double a, double b, double acc, double eps, double f2, double f3, int rec_num);

double adapt(double f(double), double a, double b, double acc, double eps);
double adapt_Clenshaw_Curtis(double f(double), double a, double b, double acc, double eps);
double my_fun(double x);
double multi_adapt_rec(double f(double), double a, double b, double acc, double eps, double f2, double f3, int rec_num);

double multi_adapt(double f(double), double a, double b, double acc, double eps);
#define MULTI_H
#endif