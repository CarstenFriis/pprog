#include"ode.h"




int main(void){
	
    double x, step;
    gsl_vector* y = gsl_vector_alloc(2);
	FILE *mystream = fopen("log", "w");
	fprintf(mystream,"A: Solving orbit differential equation from practical programming with u(0)=1,u'(0)=0.5\n");
	fprintf(mystream,"See plot for result.\n");

    for (double b = 0; b < 20*M_PI; b+=0.1) {
        gsl_vector_set(y, 0, 1);
        gsl_vector_set(y, 1, 0.5);
        step = 1e-3;
        x = 0;

        driver(&rkfstep45,&orbit,&x, b, step, y, 1e-6, 1e-6);
        printf("%g %g %g\n", x, gsl_vector_get(y, 0), gsl_vector_get(y, 1));
    }
    printf("%g\n\n",x);
    gsl_vector* y_res_1 = gsl_vector_alloc(2);
    gsl_vector_memcpy(y_res_1,y);
    printf("\n\n");
    printf("\n\n");
    printf("\n\n");
	fprintf(mystream,"B: Storing path for same orbit differential equation with b=62 \n");
    int iter;

        gsl_vector_set(y, 0, 1);
        gsl_vector_set(y, 1, 0.5);
        double b = 62.8;
        step = 1e-3;
        x = 0.0;
        gsl_matrix* path = gsl_matrix_calloc(1000,3);
        iter = driver_path(&rkfstep45,&orbit,&x, b, step, y, 1e-5, 1e-5,path);

    printf("\n\n");
    printf("\n\n");
            for (int i= 0; i<iter; i++) {
        printf("%g %g \n", gsl_matrix_get(path,i,0), gsl_matrix_get(path,i,1));
                     }
    
		fprintf(mystream,"number of steps/path elements = %d\n",iter);
		fprintf(mystream,"The user is responsible for giving sufficiently big matrix to store the path.\n");

        fprintf(mystream,"t=%g y=%g y'=%g\n", x, gsl_vector_get(y, 0), gsl_vector_get(y, 1));
    gsl_vector_free(y);
    gsl_vector_free(y_res_1);
    gsl_matrix_free(path);
	fclose(mystream);
	return 0;
}