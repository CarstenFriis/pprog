#ifndef ODE_H  /* for multiple includes */
#include"../linear_eq/linear_eq.h"

void orbit(double x, gsl_vector* y, gsl_vector* dydx);
void harm_osc(double x, gsl_vector* y, gsl_vector* dydx);
void rkfstep45(void f(double x, gsl_vector* y, gsl_vector* dydx), double x, double step, gsl_vector* yx, gsl_vector* yx_step,gsl_vector* error);


void driver(void stepper(void f(double x, gsl_vector* y, gsl_vector* dydx), 
double x, double step, gsl_vector* yx, gsl_vector* yx_step,gsl_vector* error),
void f(double x, gsl_vector* y, gsl_vector* dydx),
double* x, double b,double step,gsl_vector* yx,double acc, double eps);


int driver_path(void stepper(void f(double x, gsl_vector* y, gsl_vector* dydx), 
double x, double step, gsl_vector* yx, gsl_vector* yx_step,gsl_vector* error),
void f(double x, gsl_vector* y, gsl_vector* dydx),
double* x, double b,double step,gsl_vector* yx,double acc, double eps, gsl_matrix* path);
#define ODE_H
#endif