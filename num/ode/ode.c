#include"ode.h"


void rkfstep45(void f(double x, gsl_vector* y, gsl_vector* dydx), double x, double step, gsl_vector* yx, gsl_vector* yx_step,gsl_vector* error){
	int n = yx->size;
	gsl_vector* k0= gsl_vector_alloc(n);
	gsl_vector* k1= gsl_vector_alloc(n);
	gsl_vector* k2= gsl_vector_alloc(n);
	gsl_vector* k3= gsl_vector_alloc(n);
	gsl_vector* k4= gsl_vector_alloc(n);
	gsl_vector* k5= gsl_vector_alloc(n);
	gsl_vector* y_temp= gsl_vector_alloc(n); //
	f(x,yx,k0);
	for(int i=0;i<n;i++){
		gsl_vector_set(y_temp,i,gsl_vector_get(yx,i)+
			1.0/4*gsl_vector_get(k0,i)*step);
	}
	f(x+1.0/4*step,y_temp,k1);
	for(int i=0;i<n;i++){
		gsl_vector_set(y_temp,i,gsl_vector_get(yx,i)+(
			3.0/32*gsl_vector_get(k0,i)+
			9.0/32*gsl_vector_get(k1,i))*step);
	}
	f(x+3.0/8*step,y_temp,k2);
	for(int i=0;i<n;i++){
		gsl_vector_set(y_temp,i,gsl_vector_get(yx,i)+(
			1932.0/2197*gsl_vector_get(k0,i)-
			7200.0/2197*gsl_vector_get(k1,i) +
			7296.0/2197*gsl_vector_get(k2,i))*step);
	}
	f(x+12.0/13*step,y_temp,k3);
	for(int i=0;i<n;i++){
		gsl_vector_set(y_temp,i,gsl_vector_get(yx,i)+(
			439.0/216*gsl_vector_get(k0,i)-
			8.0*gsl_vector_get(k1,i) +
			3680.0/513*gsl_vector_get(k2,i)- 
			845.0/4104*gsl_vector_get(k3,i))*step);
	}
	f(x+1.0*step,y_temp,k4);
	for(int i=0;i<n;i++){
		gsl_vector_set(y_temp,i,gsl_vector_get(yx,i)+(
			-8.0/27*gsl_vector_get(k0,i)+
			2.0*gsl_vector_get(k1,i) -
			3544.0/2565*gsl_vector_get(k2,i)+
			1859.0/4104*gsl_vector_get(k3,i)- 
			11.0/40*gsl_vector_get(k4,i))*step);
	}
	f(x+1.0/2*step,y_temp,k5);
	for(int i =0;i<n;i++){
		gsl_vector_set(yx_step,i,gsl_vector_get(yx,i)+(
			16.0/135*gsl_vector_get(k0,i)+
			6656.0/12825*gsl_vector_get(k2,i)+
			28561.0/56430*gsl_vector_get(k3,i)-
			9.0/50*gsl_vector_get(k4,i)+ 
			2.0/55*gsl_vector_get(k5,i))*step);
		
		gsl_vector_set(y_temp,i,gsl_vector_get(yx,i)+(
			25.0/216*gsl_vector_get(k0,i)+
			1408.0/2565*gsl_vector_get(k2,i)+
			2197.0/4104*gsl_vector_get(k3,i)-
			1.0/5*gsl_vector_get(k4,i))*step);
		gsl_vector_set(error,i,gsl_vector_get(yx_step,i)-gsl_vector_get(y_temp,i));
	}
		
	gsl_vector_free(k0);
	gsl_vector_free(k1);
	gsl_vector_free(k2);
	gsl_vector_free(k3);
	gsl_vector_free(k4);
	gsl_vector_free(k5);
	gsl_vector_free(y_temp);
}


void driver(void stepper(void f(double x, gsl_vector* y, gsl_vector* dydx), 
double x, double step, gsl_vector* yx, gsl_vector* yx_step,gsl_vector* error),
void f(double x, gsl_vector* y, gsl_vector* dydx),
double* x, double b,double step,gsl_vector* yx,double acc, double eps){
	double a=*x;
	int n=yx->size;
	double tau, y_norm, error_norm;
	gsl_vector* yh = gsl_vector_alloc(n);
    gsl_vector* error = gsl_vector_alloc(n);
	int iter = 0;
	do{
		if (fabs(*x + step) > fabs(b)) {step = b - *x;}
		//if(*x+*step>b) *step=b-*x;
		if(*x>=b) break;
		stepper(f,*x,step,yx,yh,error);
		gsl_blas_ddot(yh,yh,(&y_norm));
		y_norm = sqrt(y_norm);
		gsl_blas_ddot(error,error,(&error_norm));
		error_norm = sqrt(error_norm);
		tau = (acc+eps*y_norm)*sqrt(step*1.0/(b-a));
		if(tau > error_norm){
			gsl_vector_memcpy(yx,yh);
			iter++;
			*x += step;
		}
		step = step*pow(tau/error_norm,0.25)*0.95;
	}while(iter < 1e6 );
}

int driver_path(void stepper(void f(double x, gsl_vector* y, gsl_vector* dydx), 
double x, double step, gsl_vector* yx, gsl_vector* yx_step,gsl_vector* error),
void f(double x, gsl_vector* y, gsl_vector* dydx),
double* x, double b,double step,gsl_vector* yx,double acc, double eps, gsl_matrix* path){
	double a=*x;
	int n=yx->size;
	double tau, y_norm, error_norm;
	gsl_vector* yh = gsl_vector_alloc(n);
    gsl_vector* error = gsl_vector_alloc(n);
	int iter = 0;
	gsl_matrix_set(path,iter,0,*x);
	int path_iter = 0;
			for(int i = 0;i<n;i++){
			gsl_matrix_set(path,iter,i+1,gsl_vector_get(yx,i));
			}
	do{
		if (fabs(*x + step) > fabs(b)) {step = b - *x;}
		if(*x>=b) {
			iter++;
			gsl_matrix_set(path,iter,0,*x);
			
			if(iter < path->size1 ){
			for(int i = 0;i<n;i++){
			gsl_matrix_set(path,iter,i+1,gsl_vector_get(yx,i));
			path_iter = iter;
			}
		}else if(iter == path->size1 ){
			fprintf(stderr,"hey, you are too small, only stored first %i steps\n",iter);
			path_iter = iter;
			}
			break;}
		stepper(f,*x,step,yx,yh,error);
		gsl_blas_ddot(yh,yh,(&y_norm));
		y_norm = sqrt(y_norm);
		gsl_blas_ddot(error,error,(&error_norm));
		error_norm = sqrt(error_norm);
		tau = (acc+eps*y_norm)*sqrt(step*1.0/(b-a));
		if(tau > error_norm){
			gsl_vector_memcpy(yx,yh);
			*x += step;
			iter++;
			gsl_matrix_set(path,iter,0,*x);
			if(iter < path->size1 ){
			for(int i = 0;i<n;i++){
			gsl_matrix_set(path,iter,i+1,gsl_vector_get(yx,i));
			path_iter = iter;
			}
		}else if(iter == path->size1 ){
			fprintf(stderr,"hey, you are too small, only stored first %i steps\n",iter);
			path_iter = iter;
			}
		}

		step = step*pow(tau/error_norm,0.25)*0.95;
	}while(iter < 1e6 );
	return path_iter;
}