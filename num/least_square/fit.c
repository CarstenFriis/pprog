#include "least_square.h"

void least_square_fit(gsl_vector * x,gsl_vector * y,gsl_vector * dy,gsl_vector* c,gsl_matrix* S,int m, double funs(int i,double x),gsl_vector* c_err){
	int n = x->size;
	gsl_vector* b = gsl_vector_alloc(n);
	gsl_matrix* A = gsl_matrix_alloc(n, m);
	gsl_matrix* R = gsl_matrix_calloc(m,m);
	gsl_matrix* I = gsl_matrix_calloc(m,m);
	gsl_matrix* R_inv = gsl_matrix_calloc(m,m);
	gsl_matrix_set_identity(I);
	for(int i = 0; i<n; i++){
		double xi = gsl_vector_get(x,i);
		double yi = gsl_vector_get(y,i);
		double dyi = gsl_vector_get(dy,i);
		gsl_vector_set(b,i,yi/dyi);
		for(int k = 0; k <m; k++){
			gsl_matrix_set(A,i,k,funs(k,xi)/dyi);
		}
	}

	qr_gs_decomp(A,R);
	qr_gs_solve(A,R,b,c);
	qr_gs_inverse(I,R,R_inv);

	gsl_blas_dgemm(CblasNoTrans,CblasTrans,1,R_inv,R_inv,0,S);
	
	for( int i = 0;i<m ; i++){
		gsl_vector_set(c_err,i,sqrt(gsl_matrix_get(S,i,i)));
	}
	gsl_vector_free(b);
	gsl_matrix_free(I);
	gsl_matrix_free(R_inv);
	gsl_matrix_free(A);
	gsl_matrix_free(R);
}