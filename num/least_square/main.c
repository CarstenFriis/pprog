#include "least_square.h"

double funs(int i, double x)
	{
		switch (i)
		{
		case 0:
			return log(x);
			break;
		case 1:
			return 1.0;
			break;
		case 2:
			return x;
			break;
		default:
		{
			fprintf(stderr, "funs: wrong i:%d", i);
			return NAN;
		}
		}
	}

int main(void)
{

	double x[] = {0.1, 1.33, 2.55, 3.78, 5, 6.22, 7.45, 8.68, 9.9};
	double y[] = {-15.3, 0.32, 2.45, 2.75, 2.27, 1.35, 0.157, -1.23, -2.75};
	double dy[] = {1.04, 0.594, 0.983, 0.998, 1.11, 0.398, 0.535, 0.968, 0.478};
	int n = sizeof(x) / sizeof(x[0]);
	gsl_vector *x_val = gsl_vector_alloc(n);
	gsl_vector *y_val = gsl_vector_alloc(n);
	gsl_vector *dy_val = gsl_vector_alloc(n);
	//printf("x \t y\t dy\t\n");


	for (int i = 0; i < n; i++)
	{	
		gsl_vector_set(x_val, i, x[i]);
		gsl_vector_set(y_val, i, y[i]);
		gsl_vector_set(dy_val, i, dy[i]);
		printf(" %8g\t %8g\t %8g\n" ,x[i],y[i], dy[i]);
	}

	printf("\n\n");
	int m = 3;
	gsl_vector* c = gsl_vector_alloc(m);
	gsl_vector* dc = gsl_vector_alloc(m);
	gsl_matrix* S = gsl_matrix_alloc(m,m);
	least_square_fit(x_val,y_val,dy_val, c,S, m, funs,dc);

	//for( int i = 0; i <m; i++){
	//	gsl_vector_set(dc,i,sqrt(gsl_matrix_get(S,i,i)));
	//}
printf("\tx \t F(x)\t F(x)+dF(x)\t F(x)-dF(x) \t dF\n");
	for(int i = 0; i<n; i++){
		double F_val=0;
		double dF_val = 0;
		double xi = gsl_vector_get(x_val,i);
		for(int k = 0; k<m; k++){
			dF_val += pow(gsl_vector_get(dc,k)*funs(k,xi),2);
			
			F_val +=gsl_vector_get(c,k)*funs(k,xi);
		}
		dF_val = sqrt(dF_val);
		printf("%9g \t %8g\t %8g\t %8g \t %g \n" ,xi,F_val,F_val + dF_val, F_val - dF_val,dF_val);
	}

	printf("\n\n");

	printf("\n\n");
	printf("Coeffiients:\n");
	gsl_vector_fprintf(stdout,c,"%g");

	printf("Part B:\n");

	matrix_printf("Covariance Matrix",S);

	printf("\n\n");

	printf("error of Coeffiients:\n");
	gsl_vector_fprintf(stdout,dc,"%g");
	gsl_vector_free(x_val);
	gsl_vector_free(y_val);
	gsl_vector_free(dy_val);

	return 0;
}