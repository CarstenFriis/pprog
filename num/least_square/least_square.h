#ifndef LEAST_SQUARE_H  /* for multiple includes */
#include "../linear_eq/linear_eq.h"


void least_square_fit(gsl_vector * x,gsl_vector * y,gsl_vector * dy,gsl_vector* c,gsl_matrix* S,int m, double funs(int i,double x),gsl_vector* c_err);
#define LEAST_SQUARE_H
#endif