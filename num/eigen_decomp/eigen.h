#ifndef HAVE_EIGEN_H  /* for multiple includes */
#include "../linear_eq/linear_eq.h"


int jac_decomp_sweep(gsl_matrix *A,gsl_matrix *V,gsl_vector* e, int descending);
int jac_decomp_eig_by_eig(gsl_matrix *A,gsl_matrix *V,gsl_vector* e,int number, int descending);
void rebuild_matrix(gsl_matrix *A);
void fill_matrix_symmetric(gsl_matrix *A);
#define HAVE_EIGEN_H
#endif