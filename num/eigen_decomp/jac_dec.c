#include "eigen.h"

int jac_rotation(gsl_matrix *A, gsl_matrix *V, gsl_vector *e, int p, int q, int descending)
{
	//descending = 1 sorts eigenvalue by descending order, any other val is just ascending
	int n = A->size1;
	double api, aqi, apq, aqq, app, c, s, api1, apq1;
	double aqi1, app1, aqq1, vip, viq, aip, aiq;
	double aip1, aiq1;
	double phi;

	int status = 0;
	aqq = gsl_vector_get(e, q);
	app = gsl_vector_get(e, p);
	apq = gsl_matrix_get(A, p, q);
	phi = 0.5 * atan2(2 * apq, aqq - app);
	if (descending == 1)
	{
		phi = -0.5 * atan2(2 * apq, -aqq + app);
	}
	c = cos(phi);
	s = sin(phi);
	app1 = c * c * app - 2 * s * c * apq + s * s * aqq;
	aqq1 = s * s * app + 2 * s * c * apq + c * c * aqq;
	apq1 = s * c * (app - aqq) + (c * c - s * s) * apq;

	if (app1 != app || aqq1 != aqq)
	{
		status = 1;
		gsl_vector_set(e, p, app1);
		gsl_vector_set(e, q, aqq1);
		gsl_matrix_set(A, p, q, 0.0);
		for (int i = 0; i < p; i++)
		{
			aip = gsl_matrix_get(A, i, p);
			aiq = gsl_matrix_get(A, i, q);
			aip1 = c * aip - s * aiq;
			aiq1 = c * aiq + s * aip;
			gsl_matrix_set(A, i, p, aip1);
			gsl_matrix_set(A, i, q, aiq1);
		}

		for (int i = p + 1; i < q; i++)
		{
			api = gsl_matrix_get(A, p, i);
			aiq = gsl_matrix_get(A, i, q);
			api1 = c * api - s * aiq;
			aiq1 = c * aiq + s * api;
			gsl_matrix_set(A, p, i, api1);
			gsl_matrix_set(A, i, q, aiq1);
		}

		for (int i = q + 1; i < n; i++)
		{
			api = gsl_matrix_get(A, p, i);
			aqi = gsl_matrix_get(A, q, i);
			api1 = c * api - s * aqi;
			aqi1 = c * aqi + s * api;
			gsl_matrix_set(A, p, i, api1);
			gsl_matrix_set(A, q, i, aqi1);
		}
		for (int i = 0; i < n; i++)
		{
			vip = gsl_matrix_get(V, i, p);
			viq = gsl_matrix_get(V, i, q);
			gsl_matrix_set(V, i, p, c * vip - s * viq);
			gsl_matrix_set(V, i, q, c * viq + s * vip);
		}
	}
	return status;
}

int jac_decomp_eig_by_eig(gsl_matrix *A, gsl_matrix *V, gsl_vector *e, int number, int descending)
{
	int n = A->size1;
	gsl_matrix_set_identity(V);
	for (int k = 0; k < n; k++)
	{
		gsl_vector_set(e, k, gsl_matrix_get(A, k, k));
	}
	int status, iter = 0, p, q;

	for (p = 0; p < number && p < n; p++)
	{
		do
		{
			iter++;
			for (q = p + 1; q < n; q++)
			{
				status = jac_rotation(A, V, e, p, q, descending);
			}
		} while (status != 0);
	}
	return iter;
}
int jac_decomp_sweep(gsl_matrix *A, gsl_matrix *V, gsl_vector *e, int descending)
{
	int n = A->size1;
	gsl_matrix_set_identity(V);
	for (int k = 0; k < n; k++)
	{
		gsl_vector_set(e, k, gsl_matrix_get(A, k, k));
	}
	int status, iter = 0, p, q;
	do
	{
		iter++;
		status = 0;
		for (p = 0; p < n; p++)
		{
			for (q = p + 1; q < n; q++)
			{
				status = jac_rotation(A, V, e, p, q, descending);
			}
		}
	} while (status != 0);
	return iter;
}

void rebuild_matrix(gsl_matrix *A)
{
	for (int i = 0; i < A->size1; i++)
	{
		for (int j = 0; j < A->size2; j++)
		{
			gsl_matrix_set(A, i, j, gsl_matrix_get(A, j, i));
		}
	}
}
void fill_matrix_symmetric(gsl_matrix *A)
{
	for (int i = 0; i < A->size1; i++)
	{
		for (int j = 0; j < A->size2; j++)
		{
			double randnum = (double)rand() / RAND_MAX * 100;
			gsl_matrix_set(A, i, j, randnum);
			gsl_matrix_set(A, j, i, randnum); /* rnd # from 1-10 */
		}
	}
}