//#include "../linear_eq/linear_eq.h"
#include <math.h>
#include"eigen.h"
#include <getopt.h>
#include <stdlib.h>
int main(int argc, char *argv[])
{
	int n = 6;



	gsl_matrix *A = gsl_matrix_calloc(n, n);
	fill_matrix_symmetric(A);
	matrix_printf("matrix A", A);


	printf("Second part\n");

	printf("Row by row eigenvalue.\n");
	
	gsl_matrix *V = gsl_matrix_calloc(n, n);
	gsl_matrix *eig_list = gsl_matrix_calloc(n, n);
	gsl_vector *e = gsl_vector_calloc(n);
	gsl_vector *sweeps_count = gsl_vector_alloc(n);;
	for(int i=0;i<n;i++){
		gsl_vector_set(sweeps_count,i,jac_decomp_eig_by_eig(A,V,e,i,0));
		rebuild_matrix(A);
		gsl_matrix_set_col(eig_list,i,e);
	}
	//matrix_printf("Initial Matrix", A);
	//int sweeps = jac_decomp_eig_by_eig(A,V,e,3,1);
	printf("Number of eigenvalues asked to find, and corresponding number of sweeps below:\n");
	printf("0\t1\t2\t3\t4\t5\n");
	printf("%g\t%g\t%g\t%g\t%g\t%g\t\n",gsl_vector_get(sweeps_count,0),gsl_vector_get(sweeps_count,1),gsl_vector_get(sweeps_count,2),gsl_vector_get(sweeps_count,3)
	,gsl_vector_get(sweeps_count,4),gsl_vector_get(sweeps_count,5));
	printf("\n\n");
	printf("The corresponding eigenvalues found:\n");
	matrix_printf("Eig",eig_list);

	printf("The eigenvalues found by full sweep(cyclic):\n");
	jac_decomp_sweep(A,V,e,0);
	gsl_vector_fprintf(stdout, e, "%g");
	printf("Notice that finding 5 eigenvalues is the same as finding all 6.\n");


	rebuild_matrix(A);
	printf("Descending order:\n");
 	jac_decomp_sweep(A,V,e,1);
	gsl_vector_fprintf(stdout, e, "%g");

	printf("Comparison:\n");
	printf("Size = 120.\n");
	n=120;
	gsl_matrix *V1 = gsl_matrix_calloc(n, n);
	gsl_vector *e1 = gsl_vector_calloc(n);
	gsl_matrix *B = gsl_matrix_calloc(n, n);
	fill_matrix_symmetric(B);
	int sweeps_full_asc = jac_decomp_sweep(B,V1,e1,0);
	rebuild_matrix(B);
	int sweeps_full_des = jac_decomp_sweep(B,V1,e1,1);
	rebuild_matrix(B);
	int sweeps_eigbyeig_single_asc = jac_decomp_eig_by_eig(B,V1,e1,1,0);
	rebuild_matrix(B);
	int sweeps_eigbyeig_single_des = jac_decomp_eig_by_eig(B,V1,e1,1,1);
	rebuild_matrix(B);
	int sweeps_eigbyeig_all_asc = jac_decomp_eig_by_eig(B,V1,e1,n,0);
	rebuild_matrix(B);
	int sweeps_eigbyeig_all_des = jac_decomp_eig_by_eig(B,V1,e1,n,1);
	rebuild_matrix(B);
	printf("Sweeps:\n");
	printf("full sweep(cyclic) ascending: %d\n",sweeps_full_asc);
	printf("eig by eig single eigenvalue ascending: %d\n",sweeps_eigbyeig_single_asc);
	printf("eig by eig all eigenvalues ascending: %d\n",sweeps_eigbyeig_all_asc);
	printf("full sweep(cyclic) descending: %d\n",sweeps_full_des);
	printf("eig by eig single eigenvalue descending: %d\n",sweeps_eigbyeig_single_des);
	printf("eig by eig all eigenvalues descending: %d\n",sweeps_eigbyeig_all_des);
	return 0;
}