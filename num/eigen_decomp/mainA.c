//#include "../linear_eq/linear_eq.h"
#include <math.h>
#include"eigen.h"
#include <getopt.h>
#include <stdlib.h>
int main(int argc, char *argv[])
{
	int n = 6;
	while (1)
	{
		struct option long_options[] =
			{
				{"size", required_argument, NULL, 'n'},
				{0,0,0,0}};
		int opt = getopt_long(argc, argv, "n", long_options, NULL);
		if (opt == -1)
			break;
		switch (opt)
		{
		case 'n':
			n = atof(optarg);
			break;
		default:
			fprintf(stderr, "Usage: %s --size n\n", argv[0]);
			exit(EXIT_FAILURE);
		}
	}

	printf("\n");

	gsl_matrix *A = gsl_matrix_calloc(n, n);
	gsl_matrix *Ac = gsl_matrix_calloc(n, n);
	
	fill_matrix_symmetric(A);
	matrix_printf("matrix A", A);
	gsl_matrix_memcpy(Ac, A);
	
	gsl_matrix *V = gsl_matrix_calloc(n, n);
	gsl_vector *e = gsl_vector_calloc(n);

	int sweeps = jac_decomp_sweep(A,V,e,0);
	//int sweeps = jac_decomp_eig_by_eig(A,V,e,3,1);
	printf("number of sweeps = %i\n\n", sweeps);
	matrix_printf("Matrix after sweeps", A);
	printf("\n\n");
	printf("Eigenvalues:\n");
	gsl_vector_fprintf(stdout, e, "%g");

	printf("\n\n");
	matrix_printf("Eigenvectors", V);
	
	printf("\n\n");
	
	
	gsl_matrix* AV = gsl_matrix_calloc(n, n);

	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, Ac, V, 0, AV);

	gsl_matrix *VTAV = gsl_matrix_calloc(n, n);


	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, V, AV, 0, VTAV);

	matrix_printf("V^T A V = " ,VTAV);
	printf("\n\n");
	printf("Eigenvalues on diagonal as expected.");
	printf("\n\n");

	return 0;
}