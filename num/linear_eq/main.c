#include <stdio.h>
#include "linear_eq.h"

int main(void)
{
	int n = 6;
	int m = 4;

	

	printf("\n\n");

	gsl_matrix *A = gsl_matrix_calloc(n, m);

	gsl_matrix *Q = gsl_matrix_calloc(n, m);


	matrix_rand_fill(A);
	matrix_printf("Tall matrix A", A);
	printf("\n\n");
	gsl_matrix_memcpy(Q, A);
	gsl_matrix *R = gsl_matrix_calloc(m, m);

	qr_gs_decomp(Q, R);
	matrix_printf("Matrix Q:", Q);
	printf("\n\n");
	matrix_printf("Matrix R:", R);
	printf("\n\n");

	gsl_matrix *QTQ = gsl_matrix_alloc(A->size2, A->size2);
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1, Q, Q, 0, QTQ);
	matrix_printf("Matrix Q^T*Q:", QTQ);
	printf("\n\n");

	gsl_matrix *QR = gsl_matrix_calloc(A->size1, A->size2);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, Q, R, 0, QR);
	matrix_printf("Matrix Q*R:", QR);
	printf("\n\n");

	gsl_matrix_free(Q);
	gsl_matrix_free(A);
	gsl_matrix_free(R);
	gsl_matrix_free(QTQ);
	gsl_matrix_free(QR);
	printf("\n");
	printf("Part 2:\n");
	printf("Solve Ax = b using QR decomposition:\n");
	n= 4;

	Q = gsl_matrix_calloc(n,n);
	A = gsl_matrix_calloc(n, n);
	matrix_rand_fill(A);


	matrix_printf("Matrix A:", A);
	printf("\n\n");
	R = gsl_matrix_calloc(n,n);
	
	gsl_vector *b = gsl_vector_calloc(n);
	gsl_vector *c = gsl_vector_calloc(n);
	gsl_vector *y = gsl_vector_calloc(n);
	vector_rand_fill(b);
	printf("\nVector:\n");
	gsl_vector_fprintf(stdout, b, "%g");
	printf("\n\n");



	gsl_matrix_memcpy(Q, A);

	qr_gs_decomp(Q, R);

	qr_gs_solve(Q, R,b,c);
	printf("c\n\n");
	gsl_vector_fprintf(stdout, c, "%g");
	printf("\n\n");
	
	printf("Solution Rx = trans(Q)*b:\n");
	printf("x:\n");
	gsl_vector_fprintf(stdout, c, "%g");

	gsl_vector_memcpy(y, c);

	gsl_vector *r = gsl_vector_calloc(n);
	gsl_blas_dgemv(CblasNoTrans, 1, A, y, 0, r);
	printf("\nAx:\n");
	gsl_vector_fprintf(stdout, r, "%g");

	printf("\nb:\n");
	gsl_vector_fprintf(stdout, b, "%g");
	printf("\n");


	gsl_matrix* B = gsl_matrix_calloc(n,n);
	qr_gs_inverse(Q, R, B);


	matrix_printf("Matrix inv(A)=B:", B);
	printf("\n\n");


	gsl_matrix* AB = gsl_matrix_calloc(n,n);
	
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A, B, 0, AB);

	matrix_printf("A*B:", AB);
	gsl_matrix_free(Q);
	gsl_matrix_free(B);
	gsl_matrix_free(AB);
	gsl_matrix_free(A);
	gsl_matrix_free(R);
	gsl_vector_free(b);
	gsl_vector_free(c);
	gsl_vector_free(r);
	gsl_vector_free(y);
	return 0;
}