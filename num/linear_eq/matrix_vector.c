#include"linear_eq.h"

void matrix_printf(char *s, const gsl_matrix *A)
{
	int i, j;
	int n, m;

	n = (*A).size1;
	m = (*A).size2;
	printf( "%s\n", s);
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++)
		{
			printf( "%8f \t ", gsl_matrix_get(A, i, j));
		}
		printf( "\n");
	}
}

double inner_product_matrix_collumns(const gsl_matrix *A,int i,const gsl_matrix *B,int j){
	double product= 0.0;
	
	for (int l = 0; l < A->size1; l++){
		product += (gsl_matrix_get(A,l,i)*gsl_matrix_get(B,l,j));
	}

	//printf(" prod = %g \n",product);
	
	return product;
}


void matrix_rand_fill(gsl_matrix * A){ /*	Fill matrix 	*/
	for (int i = 0 ; i < A->size1 ; i++){
		for (int j = 0 ; j < A->size2 ; j++){
			gsl_matrix_set(A,i,j, (double) rand()/RAND_MAX*100); /* rnd # from 1-10 */
		}
	}
}

void vector_rand_fill(gsl_vector * b){
	for (int i = 0 ; i < b->size ; i++){
		gsl_vector_set(b,i, (double) rand()/RAND_MAX*100);
	}
}