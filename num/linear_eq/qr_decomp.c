
#include "linear_eq.h"

void qr_gs_decomp(gsl_matrix *A, gsl_matrix *R)
{
	int n = A->size1, m = A->size2;
	for (int i = 0; i < m; i++)
	{
		//matrix_printf( "Matrix A:", A);
		gsl_matrix_set(R, i, i, sqrt(inner_product_matrix_collumns(A, i, A, i)));
		//matrix_printf( "Matrix R:", R);
		for (int l = 0; l < n; l++)
		{
			gsl_matrix_set(A, l, i,
						   gsl_matrix_get(A, l, i) / gsl_matrix_get(R, i, i));

			//matrix_printf( "Matrix Q:", Q);
		}
		for (int j = i + 1; j < m; j++)
		{
			gsl_matrix_set(R, i, j, (inner_product_matrix_collumns(A, i, A, j)));
			//matrix_printf( "Matrix R:", R);
			for (int l = 0; l < n; l++)
			{
				gsl_matrix_set(A, l, j,
							   gsl_matrix_get(A, l, j) - gsl_matrix_get(A, l, i) * gsl_matrix_get(R, i, j));
			}
		}
	}
}
void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R,gsl_vector* b,gsl_vector* x){
	int n=R->size1;
	gsl_vector *c = gsl_vector_alloc(n);
	gsl_blas_dgemv(CblasTrans, 1.0, Q, b, 0, c);
	for (int i = n - 1; i >= 0; i--)
	{
		double ci = gsl_vector_get(c, i);
		for (int k = i + 1; k < n; k++)
		{
			ci -= gsl_matrix_get(R, i, k) * gsl_vector_get(x, k);
		}
		gsl_vector_set(x, i, ci / gsl_matrix_get(R, i, i));
	}
	gsl_vector_free(c);
}
void qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix* R,gsl_matrix* B){
		
	int n = Q->size1;
	gsl_vector* e = gsl_vector_calloc(n);
	gsl_vector* b = gsl_vector_calloc(n);

	for(int i = 0; i<n ; i++){
		gsl_vector_set(e,i,1.0);
		qr_gs_solve( Q,  R,e,b);
		gsl_matrix_set_col(B,i,b);
		gsl_vector_set_all(e,0.0);
		}

	gsl_vector_free(e);
	gsl_vector_free(b);

}