#ifndef HAVE_LINEAR_EQ_H  /* for multiple includes */
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>


void matrix_printf(char *s, const gsl_matrix *A);

double inner_product_matrix_collumns(const gsl_matrix *A,int i,const gsl_matrix *B,int j);
void qr_gs_decomp(gsl_matrix *A, gsl_matrix *R);
void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R,gsl_vector* b,gsl_vector* x);
void vector_rand_fill(gsl_vector * b);
void matrix_rand_fill(gsl_matrix * A);
void qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix* R,gsl_matrix* B);
#define HAVE_LINEAR_EQ_H
#endif