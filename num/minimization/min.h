#ifndef MIN_H  /* for multiple includes */
#include"../linear_eq/linear_eq.h"

/*	Functions	*/
double Rosenbrock(gsl_vector* p, gsl_vector* gradient);
double Himmelblau(gsl_vector* p, gsl_vector* gradient);

void Rosenbrock_hessian(gsl_vector * p, gsl_matrix * H);
void Himmelblau_hessian(gsl_vector * p, gsl_matrix * H);

double expo_decay(double t, gsl_vector *p); 
double expo_decay_min(gsl_vector *p, gsl_vector* gradient);



void backtracking_linesearch(
	double f(gsl_vector* p, gsl_vector* gradient),
	gsl_vector * x, gsl_vector * Dx);
void backtracking_linesearch_qn(
	double f(gsl_vector* p, gsl_vector* gradient),
	gsl_matrix * H, gsl_vector * x, gsl_vector * Dx);
	
int newton(
double f(gsl_vector* p, gsl_vector* gradient),
void hessian(gsl_vector * p, gsl_matrix * H),
gsl_vector* x,
double accuracy);
int quasi_newton(
double f(gsl_vector* p, gsl_vector* gradient),
gsl_vector* x, 
double accuracy);

#define MIN_H
#endif