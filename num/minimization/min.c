#include "min.h"

void backtracking_linesearch(
	double f(gsl_vector *p, gsl_vector *gradient),
	gsl_vector *x, gsl_vector *Dx)
{
	int n = x->size;

	gsl_vector *gradient = gsl_vector_alloc(n);
	gsl_vector *gradientz = gsl_vector_alloc(n);
	gsl_vector *z = gsl_vector_alloc(n);

	double fx = f(x, gradient), fz;

	double lambda = 1, lambda_min = 0.02;

	while (1)
	{

		gsl_vector_memcpy(z, x);
		gsl_vector_add(z, Dx);

		fz = f(z, gradientz);
		double b;
		gsl_blas_ddot(Dx, gradient, &b);
		if (fz < fx + 1e-4 * lambda * b || lambda_min < lambda_min){
			break;}
		lambda_min *= 0.5;
		gsl_vector_scale(Dx, 0.5);
	}
	gsl_vector_memcpy(x, z);

	gsl_vector_free(gradient);
	gsl_vector_free(gradientz);
	gsl_vector_free(z);
}

void backtracking_linesearch_qn(
	double f(gsl_vector *p, gsl_vector *gradient),
	gsl_matrix *H, gsl_vector *x, gsl_vector *Dx)
{

	int n = x->size;

	gsl_vector* gradient = gsl_vector_alloc(n);
	gsl_vector* gradientz = gsl_vector_alloc(n);
	gsl_vector* z  = gsl_vector_alloc(n);

	double fx = f(x,gradient), fz;
	double lambda = 1, lambda_min = 0.02;
	
	while(1){
		
		gsl_vector_memcpy(z,x); 
		gsl_vector_add(z,Dx);

		fz = f(z,gradientz);
		
		double b;
		gsl_blas_ddot(Dx, gradient, &b);
		if (fz < fx + 1e-4 * lambda * b)break;
		if( lambda_min < lambda_min){
			gsl_matrix_set_identity(H); 
			break;
		}

		lambda*=0.5; 
		gsl_vector_scale(Dx,0.5);
		}
	gsl_vector_memcpy(x,z);

	gsl_vector_free(gradient); gsl_vector_free(gradientz);
	gsl_vector_free(z);
}

int newton(
	double f(gsl_vector *p, gsl_vector *gradient), /* f: objective function, gradient: gradient, H: Hessian matrix H*/
	void hessian(gsl_vector *p, gsl_matrix *H),
	gsl_vector *x,   /* starting point, becomes the latest approximation to the root on exit */
	double accuracy) /* accuracy goal, on exit |gradient|<eps */
{
	int steps = 0;
	int n = x->size;
	gsl_matrix *H = gsl_matrix_alloc(n, n);
	gsl_matrix *R = gsl_matrix_alloc(n, n);
	gsl_vector *Dx = gsl_vector_alloc(n);
	gsl_vector *gradient = gsl_vector_alloc(n);

	do
	{
		hessian(x, H);
		f(x, gradient);
		qr_gs_decomp(H, R);

		qr_gs_solve(H, R, gradient, Dx);
		gsl_vector_scale(Dx, -1);

		backtracking_linesearch(f, x, Dx);
		f(x, gradient);
		steps++;

	} while (gsl_blas_dnrm2(gradient) > accuracy && steps < 1000);

	return steps;
}

int quasi_newton(
	double f(gsl_vector *p, gsl_vector *gradient),
	gsl_vector *x,								  
	double accuracy)
{
	int n = x->size;

	gsl_matrix *H = gsl_matrix_alloc(n, n);
	gsl_vector *grad_fx = gsl_vector_alloc(n);
	gsl_vector *y = gsl_vector_alloc(n);
	gsl_vector *s = gsl_vector_alloc(n);
	gsl_vector *Hy = gsl_vector_alloc(n);

	double Hys;
	int steps = 0;
	int step_max = 100000;

	gsl_matrix_set_identity(H);
	f(x, grad_fx);

	do
	{
		gsl_blas_dgemv(CblasNoTrans, -1.0, H, grad_fx, 0.0, s);
		
		backtracking_linesearch_qn(f, H, x, s);

		f(x, y); 
		gsl_vector_sub(y, grad_fx);

		gsl_blas_dgemv(CblasNoTrans, 1.0, H, y, 0.0, Hy);	 
		gsl_blas_dgemv(CblasNoTrans, 1.0, H, s, 0.0, grad_fx);
		gsl_blas_ddot(Hy, s, &Hys);							  
		gsl_vector_sub(s, Hy);								  

		gsl_blas_dger(1 / Hys, s, grad_fx, H);

		f(x, grad_fx); 
		steps++;
	} while (gsl_blas_dnrm2(grad_fx) > accuracy && steps < step_max);

	if (steps == step_max)
		fprintf(stderr, "Did not converge\n");

	gsl_matrix_free(H);
	gsl_vector_free(grad_fx);
	gsl_vector_free(y);
	gsl_vector_free(s);
	gsl_vector_free(Hy);

	return steps;
}