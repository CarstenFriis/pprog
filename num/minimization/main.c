#include"min.h"

int main(void){



	int steps, n = 2;
	double f,accuracy = 1e-6;
	gsl_vector * x = gsl_vector_alloc(n);
	gsl_vector * df= gsl_vector_alloc(n);
	gsl_vector_set_zero(x);

	fprintf(stdout, "\nRosenbrock minimum:\n");
	f = Rosenbrock(x,df);
	printf("Initial x\n");
	gsl_vector_fprintf(stdout, x, "%g");
	fprintf(stdout, "Rosenbrock(x) = %g\n",f);
	steps = newton(Rosenbrock, Rosenbrock_hessian, x, accuracy);
	f = Rosenbrock(x,df);
	printf("Rosenbrock minimum x\n");
	gsl_vector_fprintf(stdout, x, "%g");
	fprintf(stdout, "minimum Rosenbrock(x) = %g\n",f);
	fprintf(stdout, "steps = %i\n", steps );


//	gsl_vector_set_zero(x); /* x = 0,0 finds maximum */
	gsl_vector_set(x,0,0.-3);
	gsl_vector_set(x,1,0.-3);
	fprintf(stdout, "\nHimmelblau minimum:\n");
	f = Himmelblau(x,df);
	printf("Initial x\n");
	gsl_vector_fprintf(stdout, x, "%g");
	fprintf(stdout, "Himmelblau(x) = %g\n",f);
	steps = newton(Himmelblau, Himmelblau_hessian, x, accuracy);
	f = Himmelblau(x,df);
	printf("Himmelblau minimum x\n");
	gsl_vector_fprintf(stdout, x, "%g");
	fprintf(stdout, "minimum Himmelblau(x) = %g\n",f);
	fprintf(stdout, "steps = %i\n", steps );


	return 0;
}