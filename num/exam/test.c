#include"ode.h"


void rkfstep45(void f(double x, gsl_vector* y, gsl_vector* dydx), double x, double step, gsl_vector* yx, gsl_vector* yx_step,gsl_vector* error){
	int n = yx->size;
	gsl_vector* k0= gsl_vector_alloc(n);
	gsl_vector* k1= gsl_vector_alloc(n);
	gsl_vector* k2= gsl_vector_alloc(n);
	gsl_vector* k3= gsl_vector_alloc(n);
	gsl_vector* k4= gsl_vector_alloc(n);
	gsl_vector* k5= gsl_vector_alloc(n);
	gsl_vector* y_temp1= gsl_vector_alloc(n); //
	gsl_vector* y_temp2= gsl_vector_alloc(n); //
	f(x,yx,k0);
	for(int i=0;i<n;i++){
		gsl_vector_set(y_temp1,i,gsl_vector_get(yx,i)+
			1.0/3*gsl_vector_get(k0,i)*step);
	}
	f(x+1.0/3*step,y_temp1,k1);
	for(int i=0;i<n;i++){
		gsl_vector_set(y_temp1,i,gsl_vector_get(yx,i)+(
			-1.0/3*gsl_vector_get(k0,i)+
			1.0*gsl_vector_get(k1,i))*step);
	}
	f(x+2.0/3*step,y_temp1,k2);
	for(int i=0;i<n;i++){
		gsl_vector_set(y_temp1,i,gsl_vector_get(yx,i)+(
			1.0*gsl_vector_get(k0,i)-
			1.0*gsl_vector_get(k1,i) +
			1.0*gsl_vector_get(k2,i))*step);
	}
	f(x+1.0*step,y_temp1,k3);
	for(int i=0;i<n;i++){
		gsl_vector_set(y_temp1,i,gsl_vector_get(yx,i)+(
			439.0/216*gsl_vector_get(k0,i)-
			8.0*gsl_vector_get(k1,i) +
			3680.0/513*gsl_vector_get(k2,i)- 
			845.0/4104*gsl_vector_get(k3,i))*step);
	}
	f(x+1.0*step,y_temp1,k4);
	for(int i=0;i<n;i++){
		gsl_vector_set(y_temp1,i,gsl_vector_get(yx,i)+(
			-8.0/27*gsl_vector_get(k0,i)+
			2.0*gsl_vector_get(k1,i) -
			3544.0/2565*gsl_vector_get(k2,i)+
			1859.0/4104*gsl_vector_get(k3,i)- 
			11.0/40*gsl_vector_get(k4,i))*step);
	}
	f(x+1.0/2*step,y_temp1,k5);
	for(int i =0;i<n;i++){
		gsl_vector_set(yx_step,i,gsl_vector_get(yx,i)+(
			1.0/8*gsl_vector_get(k0,i)+
			3.0/8*gsl_vector_get(k1,i)+
			3.0/8*gsl_vector_get(k2,i)+
			1.0/8*gsl_vector_get(k3,i))*step);

		gsl_vector_set(error,i,gsl_vector_get(yx_step,i)-gsl_vector_get(y_temp1,i));
	}
		
	gsl_vector_free(k0);
	gsl_vector_free(k1);
	gsl_vector_free(k2);
	gsl_vector_free(k3);
	gsl_vector_free(k4);
	gsl_vector_free(k5);
	gsl_vector_free(y_temp1);
	gsl_vector_free(y_temp2);
}


void driver(void stepper(void f(double x, gsl_vector* y, gsl_vector* dydx), 
double x, double step, gsl_vector* yx, gsl_vector* yx_step,gsl_vector* error),
void f(double x, gsl_vector* y, gsl_vector* dydx),
double* x, double b,double* step,gsl_vector* yx,double acc, double eps){
	int n=yx->size;
	double tau, y_norm, error_norm;
	gsl_vector* yh = gsl_vector_alloc(n);
    gsl_vector* error = gsl_vector_alloc(n);
	int iter = 0;
	do{
		if (fabs(*x + *step) > fabs(b)) {*step = b - *x;}
		//if(*x+*step>b) *step=b-*x;
		if(*x>=b) break;
		stepper(f,*x,*step,yx,yh,error);
		gsl_blas_ddot(yh,yh,(&y_norm));
		y_norm = sqrt(y_norm);
		gsl_blas_ddot(error,error,(&error_norm));
		error_norm = sqrt(error_norm);
		tau = (acc+eps*y_norm);
		if(tau > error_norm){
			gsl_vector_memcpy(yx,yh);
			iter++;
			*x += *step;
		}
		*step = *step*pow(tau/error_norm,0.25)*0.95;
	}while(iter < 1e6 );
}