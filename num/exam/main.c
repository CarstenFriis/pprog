#include"exam.h"




int main(void){
	int dim = 2;
    double x, step;
    gsl_vector* y = gsl_vector_alloc(dim);
	FILE *mystream = fopen("log", "w");
	fprintf(mystream,"Practical Programming and Numerical Methods Exam Project\n");
	fprintf(mystream,"Carsten Friis Jensen\n");
	fprintf(mystream,"3/8-rule Runge-Kutta ODE stepper with step-doubling error estimate (Runge principle) \n");
	fprintf(mystream,"Done by adding an extra layer to the stepper in exam.c:   rkfstep3o8, which calls the actual stepper 3 times with different step length. \n");

	fprintf(mystream,"Solving orbit differential equation from practical programming u(φ)'' + u(φ) = 1 + εu(φ)2, with with  ε=0,u(0)=1,u'(0)=0.5\n");
	fprintf(mystream,"From x=0 to x=62.8\n");
	fprintf(mystream,"See plot for result.\n");
    int iter =0;
    for (double b = 0; b < 20*M_PI; b+=0.1) {
        gsl_vector_set(y, 0, 1);
        gsl_vector_set(y, 1, 0.5);
        step = 1e-4;
        x = 0.0;
        
        iter =driver(&rkfstep3o8,&orbit,&x, b, step, y, 1e-6, 1e-6);
        printf("%g %g %g\n", x, gsl_vector_get(y, 0), gsl_vector_get(y, 1));
   }

    printf("\n\n\n");

    fprintf(mystream,"For initial x=0,u(0)=1,u'(0)=0.5, the value at x=62.8 becomes:\n");
	fprintf(mystream,"x=%g y=%g y'%g\n", x, gsl_vector_get(y, 0), gsl_vector_get(y, 1));
	fprintf(mystream,"by %d iterations\n", iter);
    

	fprintf(mystream,"Have also made another driver from the examination project: \n");
	fprintf(mystream,"ODE: a more advanced step-size control\n");
	fprintf(mystream,"Called driver_advanced. \n");
	fprintf(mystream,"Solving the same problem as above, it returns: \n");
        gsl_vector_set(y, 0, 1);
        gsl_vector_set(y, 1, 0.5);
        double b = 62.8;
        step = 1e-3;
        x = 0.0;
        iter =driver_advanced(&rkfstep3o8,&orbit,&x, b, step, y, 1e-4, 1e-4);
        printf("%g %g %g\n", x, gsl_vector_get(y, 0), gsl_vector_get(y, 1));
	fprintf(mystream,"x=%g y=%g y'=%g\n", x, gsl_vector_get(y, 0), gsl_vector_get(y, 1));
	fprintf(mystream,"by %d iterations\n", iter);
	fclose(mystream);
	return 0;
}