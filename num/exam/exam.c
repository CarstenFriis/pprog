#include"exam.h"

void rkfstep3o8(void f(double x, gsl_vector* y, gsl_vector* dydx), double x, double step, gsl_vector* yx, gsl_vector* yx_step,gsl_vector* error){
	int n = yx->size;


	gsl_vector* y_full_step= gsl_vector_alloc(n); 
	gsl_vector* y_half_step1= gsl_vector_alloc(n); 
	rkfstep3o8inner(f, x, step, yx,  y_full_step);
	rkfstep3o8inner(f, x, step/2.0, yx,  y_half_step1);
	rkfstep3o8inner(f, x+step/2.0, step/2.0, y_half_step1,  yx_step);
	for(int i =0;i<n;i++){
	gsl_vector_set(error,i,(gsl_vector_get(y_full_step,i)-gsl_vector_get(yx_step,i))/(pow(2.0,4)-1));
	}

}
void rkfstep3o8inner(void f(double x, gsl_vector* y, gsl_vector* dydx), double x, double step, gsl_vector* yx, gsl_vector* yx_step){
	int n = yx->size;
	gsl_vector* k0= gsl_vector_alloc(n);
	gsl_vector* k1= gsl_vector_alloc(n);
	gsl_vector* k2= gsl_vector_alloc(n);
	gsl_vector* k3= gsl_vector_alloc(n);
	gsl_vector* y_temp= gsl_vector_alloc(n); //
	f(x,yx,k0);
	for(int i=0;i<n;i++){
		gsl_vector_set(y_temp,i,gsl_vector_get(yx,i)+
			1.0/3*gsl_vector_get(k0,i)*step);
	}
	f(x+1.0/3*step,y_temp,k1);
	for(int i=0;i<n;i++){
		gsl_vector_set(y_temp,i,gsl_vector_get(yx,i)+(
			-1.0/3*gsl_vector_get(k0,i)+
			1.0*gsl_vector_get(k1,i))*step);
	}
	f(x+2.0/3*step,y_temp,k2);
	for(int i=0;i<n;i++){
		gsl_vector_set(y_temp,i,gsl_vector_get(yx,i)+(
			1.0*gsl_vector_get(k0,i)-
			1.0*gsl_vector_get(k1,i) +
			1.0*gsl_vector_get(k2,i))*step);
	}
	f(x+1.0*step,y_temp,k3);
	for(int i =0;i<n;i++){
		gsl_vector_set(yx_step,i,gsl_vector_get(yx,i)+(
			1.0/8*gsl_vector_get(k0,i)+
			3.0/8*gsl_vector_get(k1,i)+
			3.0/8*gsl_vector_get(k2,i)+
			1.0/8*gsl_vector_get(k3,i))*step);

	}
	gsl_vector_free(k0);
	gsl_vector_free(k1);
	gsl_vector_free(k2);
	gsl_vector_free(k3);
	gsl_vector_free(y_temp);
}

int driver(void stepper(void f(double x, gsl_vector* y, gsl_vector* dydx), 
double x, double step, gsl_vector* yx, gsl_vector* yx_step,gsl_vector* error),
void f(double x, gsl_vector* y, gsl_vector* dydx),
double* x, double b,double step,gsl_vector* yx,double acc, double eps){
	int n=yx->size;
	double y_norm, error_norm;
	gsl_vector* yh = gsl_vector_alloc(n);
    gsl_vector* error = gsl_vector_alloc(n);
	int iter = 0;
	double a=*x;
	do{
		if (fabs(*x + step) > fabs(b)) {step = b - *x;}
		if(*x>=b) break;
		stepper(f,*x,step,yx,yh,error);
		gsl_blas_ddot(yh,yh,(&y_norm));
		y_norm = sqrt(y_norm);
		gsl_blas_ddot(error,error,(&error_norm));
		error_norm = sqrt(error_norm);
		double tau = (acc+eps*y_norm)*sqrt(step*1.0/(b-a));
		if(tau > error_norm){
			gsl_vector_memcpy(yx,yh);
			iter++;
			*x += step;
		}
		step = step*pow(tau/error_norm,0.25)*0.95;
	}while(iter < 5000 );
	gsl_blas_ddot(error,error,(&error_norm));
		error_norm = sqrt(error_norm);
		return iter;
}

int driver_advanced(void stepper(void f(double x, gsl_vector* y, gsl_vector* dydx), 
double x, double step, gsl_vector* yx, gsl_vector* yx_step,gsl_vector* error),
void f(double x, gsl_vector* y, gsl_vector* dydx),
double* x, double b,double step,gsl_vector* yx,double acc, double eps){
	int n=yx->size;
	gsl_vector* y_abs = gsl_vector_alloc(n);
	gsl_vector* e_abs = gsl_vector_alloc(n);
	gsl_vector* tau = gsl_vector_alloc(n);
	gsl_vector* tau_div_err = gsl_vector_alloc(n);
	double a=*x;
	gsl_vector* yh = gsl_vector_alloc(n);
    gsl_vector* error = gsl_vector_alloc(n);
	int iter = 0;
	do{
		if (fabs(*x + step) > fabs(b)) {step = b - *x;}
		if(*x>=b) break;
		stepper(f,*x,step,yx,yh,error);
		int step_check=0;
		for(int i=0;i<n;i++){
			double y_abs_i = sqrt(gsl_vector_get(yh,i)*gsl_vector_get(yh,i));
			double e_abs_i = sqrt(gsl_vector_get(error,i)*gsl_vector_get(error,i));
			double tau_i = (y_abs_i*eps+acc)*sqrt(step*1.0/(b-a));
			gsl_vector_set(y_abs,i,y_abs_i);
			gsl_vector_set(e_abs,i,e_abs_i);
			gsl_vector_set(tau,i,tau_i);
			gsl_vector_set(tau_div_err,i,tau_i/e_abs_i);
			if(gsl_vector_get(tau,i)<e_abs_i) {
				step_check++;
			}
		}
		if(step_check==0){
			gsl_vector_memcpy(yx,yh);
			iter++;
			*x += step;
			
		}else {
			double min_diff = gsl_vector_min(tau_div_err);
			step = step*pow(min_diff,0.25)*0.95;
			}
	}while(iter < 500000 );
	gsl_vector_free(y_abs);
	gsl_vector_free(e_abs );
	gsl_vector_free(tau );
	gsl_vector_free(tau_div_err);
	gsl_vector_free(yh );
    gsl_vector_free(error);
		return iter;
}