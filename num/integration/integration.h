#ifndef INTEGRATION_H  /* for multiple includes */
#include"../linear_eq/linear_eq.h"
double adapt_rec(double f(double), double a, double b, double acc, double eps, double f2, double f3, int rec_num);

double adapt(double f(double), double a, double b, double acc, double eps);
double adapt_Clenshaw_Curtis(double f(double), double a, double b, double acc, double eps);
double my_sqrt(double x);

double inv_sqrt(double x);
double inv_sqrt1(double x);
double fun3(double x);

double fun4(double x);
double fun5(double x);
#define INTEGRATION_H
#endif