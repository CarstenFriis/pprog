#include"integration.h"


int main(void){
	int func_calls = 0;

double my_sqrt(double x){
	func_calls++;
	return sqrt(x);
}

double inv_sqrt(double x){
	func_calls++;
	return 1.0/sqrt(x);
}
double inv_sqrt1(double x){
	func_calls++;
	return 1.0/sqrt(x+1);
}
double fun3(double x){
	func_calls++;
	return log(x)/sqrt(x);
}

double fun5(double x){
	func_calls++;
	return exp(-5*fabs(x));
}

	printf("Part A.\n");
	double a = 0,b=1,acc=1e-6,eps=1e-6;
	double sq_int = adapt(my_sqrt,a,b,acc,eps);
	printf("sqrt(x) from 0 to 1, should be 2/3\n");
	printf("is=%g\n",sq_int);
	printf("number of function calls:%d\n",func_calls);


	func_calls = 0;
	double inv_sq_int = adapt(inv_sqrt,a,b,acc,eps);
	printf("1/sqrt(x) from 0 to 1, should be 2\n");
	printf("is=%g\n",inv_sq_int);
	printf("number of function calls:%d\n",func_calls);


	func_calls = 0;
	double fun3_int = adapt(fun3,a,b,acc,eps);
	printf("ln(x)/sqrt(x) from 0 to 1 should be -4\n");
	printf("is=%g\n",fun3_int);
	printf("number of function calls:%d\n",func_calls);
	printf("Part B.\n");
	a=0;
	b=INFINITY;
	func_calls = 0;
	double fun_inf_upper = adapt(fun5,a,b,acc,eps);
	printf("exp(-5*fabs(x)) from 0 to inf, should be 0.2:\n");
	printf("is=%g\n",fun_inf_upper);
	printf("number of function calls:%d\n",func_calls);

	b=0;
	a=-INFINITY;
	func_calls = 0;
	double fun_inf_lower = adapt(fun5,a,b,acc,eps);
	printf("exp(-5*fabs(x)) from -inf to 0, should be 0.2:\n");
	printf("is=%g\n",fun_inf_lower);
	printf("number of function calls:%d\n",func_calls);


	b=INFINITY;
	a=-INFINITY;
	func_calls = 0;
	double fun_inf_both = adapt(fun5,a,b,acc,eps);
	printf("exp(-5*fabs(x)) from -inf to inf, should be 0.4:\n");
	printf("is=%g\n",fun_inf_both);
	printf("number of function calls:%d\n",func_calls);

	b=1;
	a=-1;
	printf("Part C.\n");
	printf(" Clenshaw–Curtis for inverse square root from -1 to 1.\n");
	func_calls = 0;
	double fun_inv_sq_cc = adapt_Clenshaw_Curtis(inv_sqrt1,a,b,acc,eps);
	printf("1/sqrt(x+1) from -1 to 1, should be 2*sqrt(2), 2.8294:\n");
	printf("is=%g\n",fun_inv_sq_cc);
	printf("number of function calls:%d\n",func_calls);
	return 0;
}