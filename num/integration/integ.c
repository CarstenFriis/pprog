#include "integration.h"

double adapt_rec(double f(double), double a, double b, double acc, double eps, double f2, double f3, int rec_num)
{
	assert(rec_num < 10000000);

	double f1 = f(a + (b - a) / 6);
	double f4 = f(a + 5 * (b - a) / 6);
	double Q = (2 * f1 + f2 + f3 + 2 * f4) / 6 * (b - a);
	double q = (f1 + f2 + f3 + f4) / 4 * (b - a);
	double tolerance = acc + eps * fabs(Q), error = fabs(Q - q);
	if (error < tolerance)
	{
		return Q;
	}
	else
	{
		//printf("%i\n",rec_num);
		double Q1 = adapt_rec(f, a, (a + b) / 2, acc / sqrt(2.), eps, f1, f2, rec_num + 1);
		double Q2 = adapt_rec(f, (a + b) / 2, b, acc / sqrt(2.), eps, f3, f4, rec_num + 1);
		return Q1 + Q2;
	}
}

double adapt(double f(double), double a, double b, double acc, double eps)
{
	if (isinf(a)==-1 && !isinf(b))
	{
		double fun_m_inf(double t)
		{
			double inner = b + t / (1 + t);
			return  f(inner) / (pow(1 + t, 2));
		}
		return adapt(fun_m_inf, -1, 0, acc, eps);
	}
	if (!isinf(a) && isinf(b)==1)
	{
		double fun_p_inf(double t)
		{
			double inner = a + t / (1 - t);
			return  f(inner) / (pow(1 - t, 2));
		}
		return adapt(fun_p_inf, 0, 1, acc, eps);
	}
	
	if (isinf(a)==-1 && isinf(b)==1)
	{
		double fun_pm_inf(double t)
		{
			double inner = (1-t)/t;
			return (f(inner)+f(-inner))/(t*t);
		}
		return adapt(fun_pm_inf, 0, 1, acc, eps);
	}
	double f2 = f(a + 2 * (b - a) / 6.0);
	double f3 = f(a + 4 * (b - a) / 6.0);
	int rec_num = 0;
	return adapt_rec(f, a, b, acc, eps, f2, f3, rec_num);
}

double adapt_Clenshaw_Curtis(double f(double), double a, double b, double acc, double eps)
{
	double anew = 0;
	double bnew = M_PI;
	double fnew(double th){
		double x = ((b-a)/2)*cos(th) + (b+a)/2;
		return (f(x)*sin(th)*(b-a)/2);
	}
	return adapt(fnew, anew, bnew, acc, eps);
}