#include "interpolation.h"



qspline *qspline_alloc(int n, double *x, double *y){ /* allocates and builds the quadratic spline */
	qspline*s = malloc(sizeof(qspline));
	(*s).n =n;
	(*s).x = malloc(n*sizeof(double));
	(*s).y = malloc(n*sizeof(double));
	(*s).b = malloc((n-1)*sizeof(double));
	(*s).c = malloc((n-1)*sizeof(double));

	double* dx = malloc((n-1)*sizeof(double)); //dx(i) = x(i+1)-x(i)
	double* dy = malloc((n-1)*sizeof(double)); 
	int i;
	for(i=0;i<n;i++){

	(*s).x[i] = x[i];
	(*s).y[i] = y[i];
	}


	for(i=0;i<n-1;i++){
		dx[i] = x[i+1]-x[i];
		dy[i] = y[i+1]-y[i];
	}
	(*s).c[0]=0;
	(*s).b[0]= dy[0]/dx[0];
	for(i = 1;i<n-2;i++){
		(*s).b[i] =(*s).b[i-1]+2*(*s).c[i-1]*dx[i-1];
		(*s).c[i] =(dy[i])/(dx[i]*dx[i])-(*s).b[i]/dx[i];// (y[i+1]-y[i])/((x[i+1]-x[i])*(x[i+1]-x[i]))-(*s).b[i]/(x[i+1]-x[i]);
	}
	(*s).c[n-2]/=2;
	(*s).b[n-2]= dy[n-2]/dx[n-2]-(*s).c[n-2]*dx[n-2];
	for(i = n-3;i>=0;i--){
		(*s).b[i] =2*dy[i]/dx[i]-(*s).b[i+1];
		(*s).c[i] = (dy[i])/(dx[i]*dx[i])-(*s).b[i]/dx[i];


	}
	free(dx);
	free(dy);
	return s;
}
	
double qspline_evaluate( qspline *s, double z){
	assert(z >= (*s).x[0] && z <= (*s).x[(*s).n-1]);
	int i = binary_search((*s).n, (*s).x, z);
	return (*s).y[i]+ (*s).b[i]*(z-(*s).x[i])+(*s).c[i]*(z-(*s).x[i])*(z-(*s).x[i]);
}

double qspline_derivative( qspline *s, double z){
	assert(z >= (*s).x[0] && z <= (*s).x[(*s).n-1]);
	int i = binary_search((*s).n, (*s).x, z);
	return (*s).b[i] + 2.0*(*s).c[i]*(z-(*s).x[i]);
}

double qspline_integral( qspline *s, double z){
	assert(z >= (*s).x[0] && z <= (*s).x[(*s).n-1]);
	int i = 0;
	double value = 0;
	double dx;
	double dy;
	while((*s).x[i+1] < z){
		dx = ((*s).x[i+1]-(*s).x[i]);
		value += (*s).y[i]*dx+(1.0/2)*(*s).b[i]*dx*dx+(1.0/3)*(*s).c[i]*dx*dx*dx;
		i++;
	}
	value += (*s).y[i]*(z-(*s).x[i])+1.0/2*(*s).b[i]*(z-(*s).x[i])*(z-(*s).x[i]) + 1.0/3*(*s).c[i]*(z-(*s).x[i])*(z-(*s).x[i])*(z-(*s).x[i]);
	return value;
}

void qspline_free(qspline *s){
	free((*s).x);
	free((*s).y);
	free((*s).b);
	free((*s).c);
}
