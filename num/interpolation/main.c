#include "interpolation.h"
#include <math.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_interp.h>
int main(void)
{

	
	double max = 6.5;
	double step = 0.7;

	double *x = malloc(max/step * sizeof(double));
	double *y = malloc(max/step * sizeof(double));
	
	double i = 0; 
	int j=0;
	for (i = 0; i < max ; i+=step)
	{
		x[j] = i;
		y[j] = sin(i);
		j++;
	printf("j = %d \n\n",j);
	}
	int n=j;
	printf("x = %g \n\n",x[n-1]);

	printf("x \t y\n");
	for (j = 0; j < n; j++)
	{
		printf("%12g  %12g\n", x[j], y[j]);
	}
	gsl_interp_accel *acc = gsl_interp_accel_alloc();
	gsl_spline *spline_lin = gsl_spline_alloc(gsl_interp_cspline, n);

	gsl_spline_init(spline_lin, x, y, n);
	
	printf("%d\n",n);
	qspline *qs = qspline_alloc(n,x, y);
	cspline *cs = cspline_alloc(n,x, y);
	printf("\n\n");

	
	printf("z \t linspl \t gsllin \t int\n");
	for(i=0; i<x[n-1];i+=0.1){
		printf("%12g %12g %12g %12g\n",i,
		lin_interp_eval(n,x,y,i), 
		gsl_spline_eval(spline_lin, i, acc),
		lin_interp_integration(n, x, y, i));
	}
	
	printf("\n\n");
	printf("z \t qspl \t qspl_int \t qspl_deri\n");
	for(i=0; i<x[n-1];i+=0.1){
		printf("%12g %12g %12g %12g \n",i,
		qspline_evaluate( qs, i),
		qspline_integral(qs, i),
		qspline_derivative(qs, i)
		);
	}
	printf("\n\n");
	printf("z \t cspl \t cspl_int \t cspl_deri\n");
	for(i=0; i<x[n-1];i+=0.1){
		printf("%12g %12g %12g %12g \n",i,
		cspline_evaluate( cs, i),
		cspline_integral(cs, i),
		cspline_derivative(cs, i)
		);
	}

	gsl_spline_free(spline_lin);
	gsl_interp_accel_free(acc);
	cspline_free(cs);
	qspline_free(qs);
	free(x);
	free(y);
	return 0;
}