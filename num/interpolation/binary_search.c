#include "interpolation.h"

int binary_search(int n, double *x, double z)
{
	int high = n - 1;
	int low = 0;
	int index = high;
	while (high - low > 1)
	{
		index = (high + low) / 2;
		double current = x[index];
		if (z > current)
		{
			low = index;
		}
		else
		{
			high = index;
		}
	}
	return low;
}
