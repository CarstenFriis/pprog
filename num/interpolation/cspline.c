#include "interpolation.h"



//typedef struct {int n; double *x, *y, *b, *c;} cspline;
cspline *cspline_alloc(int n, double *x, double *y){ /* allocates and builds the quadratic spline */
	cspline*s = malloc(sizeof(cspline));
	(*s).n =n;
	(*s).x = malloc(n*sizeof(double));
	(*s).y = malloc(n*sizeof(double));
	(*s).b = malloc((n-1)*sizeof(double));
	(*s).c = malloc((n-1)*sizeof(double));
	(*s).d = malloc((n-1)*sizeof(double));

	double* dx = malloc((n-1)*sizeof(double)); //dx(i) = x(i+1)-x(i)
	double* dy = malloc((n-1)*sizeof(double)); 
	double* p = malloc((n-1)*sizeof(double)); 
	int i;

	for(i=0;i<n;i++){

	(*s).x[i] = x[i];
	(*s).y[i] = y[i];
	}
	for(i=0;i<n-1;i++){
		dx[i] = x[i+1]-x[i];
		dy[i] = y[i+1]-y[i];
		p[i] = dy[i]/dx[i];
	}

	// Build B matrix, 3 vectors. Tridiagonal
	double* D = malloc((n)*sizeof(double)); //dx(i) = x(i+1)-x(i)
	double* Q = malloc((n-1)*sizeof(double)); 
	double* B = malloc((n)*sizeof(double));
	D[0] = 2;
	Q[0] = 1;
	B[0] = 3*p[0];
	for(i = 0;i<n-1;i++){
		Q[i+1] = dx[i]/dx[i+1]; 
		D[i+1] = 2*Q[i+1]+2;
		B[i+1] = 3*(p[i] + p[i+1]*Q[i+1]);
	}
	D[n-1] = 2;
	B[n-1] = 3*p[n-2];


	for(i=1;i<n;i++){
		D[i] -= Q[i-1]/D[i-1];
		B[i] -= B[i-1]/D[i-1];
	}

	(*s).b[n-1] = B[n-1]/D[n-1];

	for(i=n-2;i>=0;i--){
		(*s).b[i] = (B[i]-Q[i]*(*s).b[i+1])/D[i];
	}

	for(i=0; i<n-1;i++){
		(*s).c[i]= (-2*(*s).b[i]-(*s).b[i+1]+3*p[i])/dx[i];
		(*s).d[i] = ((*s).b[i]+(*s).b[i+1]-2*p[i])/(dx[i]*dx[i]);
	}
	
	printf("i=%g\n",(*s).b[n-1]);
	printf("z=%g\n",(*s).c[n-1]);
	printf("xi=%g\n",(*s).d[n-1]);
return s;
}
	
double cspline_evaluate( cspline *s, double z){
	assert(z >= (*s).x[0] && 	z <= (*s).x[(*s).n-1]);
	
	int i = binary_search((*s).n, (*s).x, z);
	double dx = z- s->x[i];
	return s-> y[i] + dx*(s->b[i]+dx*(s->c[i]+dx*s->d[i]));
	
}

double cspline_derivative( cspline *s, double z){
	assert(z >= (*s).x[0] && z <= (*s).x[(*s).n-1]);
	int i = binary_search((*s).n, (*s).x, z);
	double zdiff = z-(*s).x[i];
	return (*s).b[i] + 2.0*(*s).c[i]*zdiff+3.0*(*s).d[i]*zdiff*zdiff;
}

double cspline_integral( cspline *s, double z){
	assert(z >= (*s).x[0] && z <= (*s).x[(*s).n-1]);
	int i = 0;
	double value = 0;
	double dx;
	while((*s).x[i+1] < z){
		dx = ((*s).x[i+1]-(*s).x[i]);
		value += (*s).y[i]*dx+(1.0/2)*(*s).b[i]*dx*dx+(1.0/3)*(*s).c[i]*dx*dx*dx+(1.0/3)*(*s).d[i]*dx*dx*dx*dx;
		i++;
	}
	double zdiff = z-(*s).x[i];

	value += (*s).y[i]*zdiff+1.0/2*(*s).b[i]*zdiff*zdiff + 1.0/3*(*s).c[i]*zdiff*zdiff*zdiff+ 1.0/4*(*s).d[i]*zdiff*zdiff*zdiff*zdiff;
	return value;
}

void cspline_free(cspline *s){
	free((*s).x);
	free((*s).y);
	free((*s).c);
	free((*s).b);
	free((*s).d);
	free(s);
}
