#ifndef HAVE_INTERPOLATION_H  /* for multiple includes */
#include<stdlib.h>
#include<stdio.h>
#include<assert.h>


double lin_interp_eval(int n, double *x, double *y, double z);
int binary_search(int n, double *x, double z);
double lin_interp_integration(int n, double *x, double *y, double z);



typedef struct {int n; double *x, *y, *b, *c;} qspline;
qspline * qspline_alloc(int n, double *x, double *y); /* allocates and builds the quadratic spline */
double qspline_evaluate( qspline *s, double z);        /* evaluates the prebuilt spline at point z */
double qspline_derivative( qspline *s, double z); /* evaluates the derivative of the prebuilt spline at point z */
double qspline_integral( qspline *s, double z);  /* evaluates the integral of the prebuilt spline from x[0] to z */
void qspline_free(qspline *s); /* free memory allocated in qspline_alloc */

typedef struct {int n; double *x, *y, *b, *c, *d;} cspline;
cspline * cspline_alloc(int n, double *x, double *y); /* allocates and builds the quadratic spline */
double cspline_evaluate( cspline *s, double z);        /* evaluates the prebuilt spline at point z */
double cspline_derivative( cspline *s, double z); /* evaluates the derivative of the prebuilt spline at point z */
double cspline_integral( cspline *s, double z);  /* evaluates the integral of the prebuilt spline from x[0] to z */
void cspline_free(cspline *s); /* free memory allocated in qspline_alloc */



#define HAVE_INTERPOLATION_H
#endif