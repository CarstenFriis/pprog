#include "interpolation.h"

double lin_interp_eval(int n, double *x, double *y, double z)
{
	assert(z >= x[0] && z <= x[n-1]);
	int i = binary_search(n, x, z);
	return y[i] + ((y[i + 1] - y[i]) / (x[i + 1] - x[i])) * (z - x[i]);
}

double lin_interp_integration(int n, double *x, double *y, double z)
{
	assert(z >= x[0] && z <= x[n -1]);
	int current_index = 0;
	int next_index = 1;
	double current_x = x[current_index];
	double next_x = x[next_index];
	double value = 0;
	double bi;
	while (next_x < z)
	{
		bi = (y[next_index] - y[current_index]) / (next_x - current_x);
		value += y[current_index] * (next_x - current_x) + bi * ((next_x * next_x - current_x * current_x) / 2 - current_x * next_x + current_x * current_x);
		current_index += 1;
		next_index += 1;
		current_x = x[current_index];
		next_x = x[next_index];
	}
	bi = (y[next_index] - y[current_index]) / (next_x - current_x);
	value += y[current_index] * (z - current_x) + bi * ((z*z - current_x * current_x) / 2 - current_x * z + current_x * current_x);
	return value;
}
